package com.example.ziffi.locationfinderfirebase.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Adapter.AddCommentsAdapter;
import com.example.ziffi.locationfinderfirebase.Model.Comment;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.Post;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppUtils;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by ziffi on 11/5/16.
 */

public class AddCommentsFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = AddCommentsFragment.class.getSimpleName();
    public static final String ADD_COMMENT_FRAGMENT_DATABASE_REFERENCE = "add_comment_fragment_database_reference";
    public static final String ADD_COMMENT_FRAGMENT_COMMENT_REFERENCE = "add_comment_fragment_comment_reference";
    public static final String POST_REFERENCE = "post_reference";
    public static final String TABLE_COMMENTS = "table_comments";
    public static final String WRITE_COMMENTS = "write_comments";
    public static final String LOGGED_IN_USER = "logged_in_user";
    public static final String POST_KEY = "post_key";
    public static final String COMMENT_TEXT = "comment_text";
    TextView tvPostUsername, tvPostTitle, tvPostBody;
    RecyclerView rvComments;
    EditText etComment;
    Button btnPost, btnNewComment;
    private DatabaseReference mDatabaseReference, mPostReference, mCommentsReference;
    FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    public static final String TABLE_POSTS = "table_post";
    String postStatusKey = "";
    ArrayList<Comment> commentArrayList = new ArrayList<>();
    AddCommentsAdapter addCommentsAdapter;
    LinearLayoutManager linearLayoutManager;


    public static AddCommentsFragment newInstance(Bundle arguments){
        AddCommentsFragment addComments = new AddCommentsFragment();
        if(arguments != null){
            addComments.setArguments(arguments);
        }
        return addComments;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_comments, container, false);
        init(view);

        return view;
    }

    public void init(View view){
        tvPostUsername = (TextView) view.findViewById(R.id.tv_post_username);
        tvPostTitle = (TextView) view.findViewById(R.id.tv_post_title);
        tvPostBody = (TextView) view.findViewById(R.id.tv_post_body);
        rvComments = (RecyclerView) view.findViewById(R.id.rv_comments);
        etComment = (EditText) view.findViewById(R.id.et_comment);
        btnPost = (Button) view.findViewById(R.id.btn_submit);
        btnNewComment = (Button) view.findViewById(R.id.btn_new_comments);
        btnNewComment.setOnClickListener(this);
        btnPost.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        ValueEventListener valueEventListener = firebaseDBHelper.AddListenerForSingleValueEvent(ADD_COMMENT_FRAGMENT_DATABASE_REFERENCE);
        mDatabaseReference.child(FirebaseDBAttributes.TABLE_POSTS).addListenerForSingleValueEvent(valueEventListener);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvComments.setLayoutManager(linearLayoutManager);
        rvComments.setAdapter(addCommentsAdapter);


        setupUI();
    }

    public void setupUI(){

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
                if(AppUtils.isStringValid(etComment.getText().toString())){
                    HashMap<String, String> paramMap = new HashMap<>();
                    paramMap.put(TABLE_COMMENTS, FirebaseDBAttributes.TABLE_COMMENTS);
                    paramMap.put(POST_KEY, postStatusKey);
                    paramMap.put(COMMENT_TEXT, etComment.getText().toString());
                    firebaseDBHelper.WriteInDB(paramMap, mDatabaseReference, WRITE_COMMENTS);

                    mCommentsReference = mDatabaseReference.child(FirebaseDBAttributes.TABLE_COMMENTS).child(postStatusKey);
                    firebaseDBHelper.ValueEventListener(mCommentsReference, ADD_COMMENT_FRAGMENT_COMMENT_REFERENCE);
                }
                break;

            case R.id.btn_new_comments:
                Bundle arg = new Bundle();
                arg.putString("post_key", postStatusKey);
                fragTansaction(AddCommentsFragment.newInstance(null), NewCommentFragment.newInstance(arg), null);
                break;
        }
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){
        if(eventBusVO != null && eventBusVO.getTag().equals(ADD_COMMENT_FRAGMENT_DATABASE_REFERENCE)){
            Post post1 = new Post();
            Object object = ((DataSnapshot)eventBusVO.getData()).getValue();
            try {
                JSONObject jsonObject = new JSONObject(object.toString());
                Iterator iterator = jsonObject.keys();
                while (iterator.hasNext()){
                    String key = (String) iterator.next();
                    postStatusKey = key;
                    JSONObject jPost = jsonObject.optJSONObject(key);
                    post1.setAuthor(jPost.optString("author"));
                    post1.setTitle(jPost.optString("title"));
                    post1.setBody(jPost.optString("body"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(post1 != null){
                tvPostUsername.setText(post1.getAuthor());
                tvPostTitle.setText(post1.getTitle());
                tvPostBody.setText(post1.getBody());
            } else {
                Toast.makeText(getActivity(), "No Post Found!", Toast.LENGTH_SHORT).show();
            }
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(ADD_COMMENT_FRAGMENT_COMMENT_REFERENCE)){

            commentArrayList.clear();
            Comment comment = ((DataSnapshot)eventBusVO.getData()).getValue(Comment.class);
            commentArrayList.add(comment);

//            if(addCommentsAdapter == null){
                addCommentsAdapter = new AddCommentsAdapter(commentArrayList, getContext());
            rvComments.setLayoutManager(linearLayoutManager);
            rvComments.setAdapter(addCommentsAdapter);
//            } else {
//                addCommentsAdapter.notifyDataSetChanged();
//            }


        }
    }
}
