package com.example.ziffi.locationfinderfirebase.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Activity.LauncherActivity;
import com.example.ziffi.locationfinderfirebase.Model.UserData;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by ziffi on 11/12/16.
 */

public class ChooseCategoryFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = ChooseCategoryFragment.class.getSimpleName();
    TextView tvChooseCategory, tvFindLocation, tvSendLocation;
    ImageView ivFindLocation, ivSendLocation;
    Animation catZoomin, catZoomout, catTextRiseup;
    public static UserData userData;
    public static boolean cameFromLogin = false;

    public static ChooseCategoryFragment newInstance(Bundle argument){
        ChooseCategoryFragment fragment = new ChooseCategoryFragment();
        if(argument != null){
            fragment.setArguments(argument);
        }
        cameFromLogin = true;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        View view = inflater.inflate(R.layout.choose_category_fragment, container, false);
        tvChooseCategory = (TextView) view.findViewById(R.id.tv_choose_category);
        tvFindLocation = (TextView) view.findViewById(R.id.tv_find_location);
        tvSendLocation = (TextView) view.findViewById(R.id.tv_send_location);
        ivFindLocation = (ImageView) view.findViewById(R.id.iv_find_location);
        ivSendLocation = (ImageView) view.findViewById(R.id.iv_send_location);
        ivFindLocation.setOnClickListener(this);
        tvFindLocation.setOnClickListener(this);
        ivSendLocation.setOnClickListener(this);
        tvSendLocation.setOnClickListener(this);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HoneyScript-Light.ttf");
        tvChooseCategory.setTypeface(typeface);

        Typeface typeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Bikinis Personal Use.ttf");
        tvSendLocation.setTypeface(typeface2);
        tvFindLocation.setTypeface(typeface2);

        if(cameFromLogin){
            startZoomingAnimation();
            setupValues();
            fetchUserConnection();

            cameFromLogin = false;
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        customizeToolbar(true);

    }

    @Override
    public void onStop() {
        super.onStop();
        customizeToolbar(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) getActivity()).hideLoaderEffect();
    }

    public void customizeToolbar(boolean flag){

        if(flag){
            ((BaseActivity) getActivity()).ivSelect.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setText("SELECT CATEGORY");
        } else {
            ((BaseActivity) getActivity()).ivSelect.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.GONE);
        }

    }

    public void setupValues(){

        if(getArguments() != null){
            String id, name, email, profilepic;

            id = getArguments().getString(AppConstants.LOGGEDIN_USER_ID);
            name = getArguments().getString(AppConstants.LOGGEDIN_USER_NAME);
            email = getArguments().getString(AppConstants.LOGGEDIN_USER_EMAIL);
            profilepic = getArguments().getString(AppConstants.LOGGEDIN_USER_PROFILE_PIC);

            userData = new UserData(id, name, email, profilepic, null);
        }
    }

    public void fetchUserConnection(){
        String userId = getArguments().getString(AppConstants.LOGGEDIN_USER_ID);
    }

    public void startZoomingAnimation(){

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                zoomin();
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        zoomout();
                        startZoomingAnimation();
                    }
                }, 800);
            }
        }, 1000);
    }

    public void zoomin(){
        if(getContext() != null){
            catZoomin = AnimationUtils.loadAnimation(getContext(), R.anim.select_category_zoomin);
            ivFindLocation.setAnimation(catZoomin);
            ivSendLocation.setAnimation(catZoomin);

            ivFindLocation.startAnimation(catZoomin);
            ivSendLocation.startAnimation(catZoomin);
        }
    }

    public void zoomout(){
        if(getContext() != null){
            catZoomout = AnimationUtils.loadAnimation(getContext(), R.anim.select_category_zoomout);
            ivFindLocation.setAnimation(catZoomout);
            ivSendLocation.setAnimation(catZoomout);

            ivFindLocation.startAnimation(catZoomout);
            ivSendLocation.startAnimation(catZoomout);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_find_location:
            case R.id.iv_find_location:
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.rl_container, SelectFriendFragment.newInstance(null), null).addToBackStack(ChooseCategoryFragment.TAG).commit();
                break;

            case R.id.tv_send_location:
            case R.id.iv_send_location:

                break;
        }
    }
}
