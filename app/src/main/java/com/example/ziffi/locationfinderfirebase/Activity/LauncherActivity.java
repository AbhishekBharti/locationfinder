package com.example.ziffi.locationfinderfirebase.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;

import com.example.ziffi.locationfinderfirebase.Fragment.SplashScreen;
import com.example.ziffi.locationfinderfirebase.R;

/**
 * Created by ziffi on 11/3/16.
 */

public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportFragmentManager().beginTransaction().replace(R.id.rl_container, SplashScreen.newInstance(), SplashScreen.TAG).commit();
    }
}
