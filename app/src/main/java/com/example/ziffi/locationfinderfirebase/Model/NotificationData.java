package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/17/16.
 */

public class NotificationData {
    final String score;
    final String time;

    public NotificationData(String score, String time) {
        this.score = score;
        this.time = time;
    }
}
