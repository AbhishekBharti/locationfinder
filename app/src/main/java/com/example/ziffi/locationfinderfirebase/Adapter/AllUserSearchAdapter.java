package com.example.ziffi.locationfinderfirebase.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.UsersAllData;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.LocationFinderApplication;
import com.example.ziffi.locationfinderfirebase.Utils.LoginHelper;
import com.example.ziffi.locationfinderfirebase.Widget.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ziffi on 11/14/16.
 */

public class AllUserSearchAdapter extends RecyclerView.Adapter<AllUserSearchAdapter.ViewHolder>{

    public interface ConnectionRequest{
        void connectUser(String userToken, String userId, int position);
        void requestedClicked(String userToken, String userId, int position, TextView textView);
        void acceptClicked(String userToken, String userId, int position, TextView textView);
        void friendClicked(String userToken, String userId, String userName, int position, TextView textView);
    }
    ArrayList<UsersAllData> userAllDataArrayList;
    Context mContext;
    ConnectionRequest connectionRequest;
    LoginHelper loginHelper = new LoginHelper();

    public AllUserSearchAdapter(ArrayList<UsersAllData> userAllDataArrayList, Context mContext, ConnectionRequest connectionRequest) {
        this.userAllDataArrayList = userAllDataArrayList;
        this.mContext = mContext;
        this.connectionRequest = connectionRequest;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position, List<Object> payloads) {

        onBindViewHolder(holder, position);
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        Bitmap profileBitmap = loginHelper.getFacebookProfilePicture(userAllDataArrayList.get(position).getFriendId());
        loginHelper.setImageFromLink(holder.rivUserProfilePic, mContext, userAllDataArrayList.get(position).getProfilePic());
        holder.tvUserName.setText(userAllDataArrayList.get(position).getUserName());
        int connectionStatus = userAllDataArrayList.get(position).getConnectionStatus();

        switch (connectionStatus){
            case 0:
                holder.tvUserConnectButton.setText("Connect");
                holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_connect_bg);
                holder.tvUserConnectButton.setPadding(40, 15, 40, 15);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.tvUserConnectButton.setElevation(5);
                }
                break;
            case 1:
                holder.tvUserConnectButton.setText("Pending");
                holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_requested_bg);
                holder.tvUserConnectButton.setPadding(40, 15, 40, 15);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.tvUserConnectButton.setElevation(5);
                }
                break;
            case 2:
                holder.tvUserConnectButton.setText("Accept/Reject");
                holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_accept_bg);
                holder.tvUserConnectButton.setPadding(40, 15, 40, 15);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.tvUserConnectButton.setElevation(10);
                }
                break;
            case 3:
                holder.tvUserConnectButton.setText("Friend");
                holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_friend_bg);
                holder.tvUserConnectButton.setPadding(40, 15, 40, 15);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.tvUserConnectButton.setElevation(0);
                }
                break;
            default:{
                holder.tvUserConnectButton.setText("Connect");
                holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_connect_bg);
                holder.tvUserConnectButton.setPadding(40, 15, 40, 15);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.tvUserConnectButton.setElevation(5);
                }
            }
        }

        if(userAllDataArrayList.get(position).getUserId().equals(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID))){
            holder.tvUserConnectButton.setText("You");
            holder.tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_self_bg);
            holder.tvUserConnectButton.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvUserConnectButton.setElevation(0);
            }
        }
    }

    @Override
    public int getItemCount() {
        return userAllDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RoundedImageView rivUserProfilePic;
        TextView tvUserName, tvUserConnectButton;

        public ViewHolder(View itemView) {
            super(itemView);

            rivUserProfilePic = (RoundedImageView) itemView.findViewById(R.id.riv_user_pic);
            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            tvUserConnectButton = (TextView) itemView.findViewById(R.id.tv_connection_status_btn);
            tvUserConnectButton.setText("Connect");
            tvUserConnectButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tv_connection_status_btn:

                    String statusText = tvUserConnectButton.getText().toString();

                    switch (statusText){
                        case "Connect":
                            connectClicked(getAdapterPosition(), tvUserConnectButton);
                            break;

                        case "Pending":
                            requestedClicked(getAdapterPosition(), tvUserConnectButton);
                            break;

                        case "Accept/Reject":
                            acceptClicked(getAdapterPosition(), tvUserConnectButton);
                            break;

                        case "Friend":
                            friendClicked(getAdapterPosition(), tvUserConnectButton);
                            break;
                    }

                    break;
            }
        }
    }

    public void connectClicked(int positionClicked, TextView tvUserConnectButton){

        String requestRecieverUserToken = userAllDataArrayList.get(positionClicked).getUserToken();
        String requestRecieverUserId = userAllDataArrayList.get(positionClicked).getUserId();

        connectionRequest.connectUser(requestRecieverUserToken, requestRecieverUserId, positionClicked);

        //temprorary Change
        tvUserConnectButton.setText("Pending");
        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_requested_bg);
        tvUserConnectButton.setPadding(40, 15, 40, 15);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvUserConnectButton.setElevation(5);
        }
    }

    public void requestedClicked(int positionClicked, TextView tvUserConnectButton){
        String requestRecieverUserToken = userAllDataArrayList.get(positionClicked).getUserToken();
        String requestRecieverUserId = userAllDataArrayList.get(positionClicked).getUserId();

        connectionRequest.requestedClicked(requestRecieverUserToken, requestRecieverUserId, positionClicked, tvUserConnectButton);
    }

    public void acceptClicked(int positionClicked, TextView tvUserConnectButton){
        String requestSenderUserToken = userAllDataArrayList.get(positionClicked).getUserToken();
        String requestSenderUserId = userAllDataArrayList.get(positionClicked).getUserId();

        connectionRequest.acceptClicked(requestSenderUserToken, requestSenderUserId, positionClicked, tvUserConnectButton);

//        tvUserConnectButton.setText("Friend");
//        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_friend_bg);
//        tvUserConnectButton.setPadding(40, 15, 40, 15);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            tvUserConnectButton.setElevation(0);
//        }
    }

    public void friendClicked(int positionClicked, TextView tvUserConnectButton){
        String requestRecieverUserToken = userAllDataArrayList.get(positionClicked).getUserToken();
        String requestRecieverUserId = userAllDataArrayList.get(positionClicked).getUserId();
        String requestRecieverUserName = userAllDataArrayList.get(positionClicked).getUserName();

        connectionRequest.friendClicked(requestRecieverUserToken, requestRecieverUserId, requestRecieverUserName, positionClicked, tvUserConnectButton);
    }

}
