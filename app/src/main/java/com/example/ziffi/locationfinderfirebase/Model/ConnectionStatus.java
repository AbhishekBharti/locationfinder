package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/17/16.
 */

public class ConnectionStatus {
    public String friendId;
    public int connectionStatus;

    public ConnectionStatus(){}

    public ConnectionStatus(String userId, int connectionStatus) {
        this.friendId = userId;
        this.connectionStatus = connectionStatus;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public int getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(int connectionStatus) {
        this.connectionStatus = connectionStatus;
    }
}
