package com.example.ziffi.locationfinderfirebase.Fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.example.ziffi.locationfinderfirebase.Utils.LocationFinderApplication;
import com.example.ziffi.locationfinderfirebase.Utils.LoginHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ziffi on 11/3/16.
 */

public class LoginFragment extends BaseFragment implements LoginHelper.LoginData {
    public static final String TAG = LoginFragment.class.getSimpleName();
    LoginButton loginButton;
    RelativeLayout rlGoogleLogin, rlSignin;
    TextView tvGTracker, tvSignUp;
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    private static DatabaseReference mDatabaseReference;
    LoginHelper loginHelper = new LoginHelper();
    private static HashMap<String, String> map;
    private static Bundle argument;

    public static LoginFragment newInstance(Bundle args){
        LoginFragment loginFragment = new LoginFragment();
        if(args != null){
            loginFragment.setArguments(args);
        }
        return loginFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View view = inflater.inflate(R.layout.login_fragment, container, false);
        loginButton = (LoginButton) view.findViewById(R.id.login_facebook);
        loginButton.setFragment(this);
        rlSignin = (RelativeLayout) view.findViewById(R.id.rl_sign_in);
        rlSignin.setVisibility(View.GONE);
        rlGoogleLogin = (RelativeLayout) view.findViewById(R.id.rl_login_gmail);
        tvGTracker = (TextView) view.findViewById(R.id.tv_g_tracker);
        tvSignUp = (TextView) view.findViewById(R.id.tv_signup);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HoneyScript-Light.ttf");
        tvGTracker.setTypeface(typeface);
        tvSignUp.setTypeface(typeface);

        startRiseupAnimation();

        mAuth = FirebaseAuth.getInstance();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mCallbackManager = CallbackManager.Factory.create();
        loginHelper.FacebookLogin(getContext(), loginButton, mCallbackManager, mAuth, mAuthListener, this);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((BaseActivity) getActivity()).hideActionBar();
        loginHelper.AddAuthStateListener();
//        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebaseDBHelper.removeValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS).child(AppConstants.LOGGEDIN_USER_ID));
        ((BaseActivity) getActivity()).showActionBar();
        loginHelper.RemoveAuthStateListener();
    }

    public void startRiseupAnimation(){
        final Animation riseLogin = AnimationUtils.loadAnimation(getActivity(), R.anim.rise_login);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rlSignin.setVisibility(View.VISIBLE);
                rlSignin.setAnimation(riseLogin);
                rlSignin.startAnimation(riseLogin);
            }
        }, 200);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void setLoginData(String id, String name, String email, String profilepic, String userToken) {

        //shared Pref
        LocationFinderApplication.setBooleanSharedPreference(AppConstants.IS_USER_LOGGEDIN, true);
        LocationFinderApplication.setStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME, name);
        LocationFinderApplication.setStringSharedPreference(AppConstants.LOGGEDIN_USER_ID, id);
        LocationFinderApplication.setStringSharedPreference(AppConstants.LOGGEDIN_USER_EMAIL, email);
        LocationFinderApplication.setStringSharedPreference(AppConstants.LOGGEDIN_USER_PROFILE_PIC, profilepic);
        LocationFinderApplication.setStringSharedPreference(AppConstants.LOGGEDIN_USER_TOKEN, userToken);


        argument = new Bundle();
        argument.putString(AppConstants.LOGGEDIN_USER_ID, id);
        argument.putString(AppConstants.LOGGEDIN_USER_NAME, name);
        argument.putString(AppConstants.LOGGEDIN_USER_EMAIL, email);
        argument.putString(AppConstants.LOGGEDIN_USER_PROFILE_PIC, profilepic);
        argument.putString(AppConstants.LOGGEDIN_USER_TOKEN, userToken);

        map = new HashMap<>();
        map.put(AppConstants.LOGGEDIN_USER_ID, id);
        map.put(AppConstants.LOGGEDIN_USER_NAME, name);
        map.put(AppConstants.LOGGEDIN_USER_EMAIL, email);
        map.put(AppConstants.LOGGEDIN_USER_PROFILE_PIC, profilepic);
        map.put(AppConstants.LOGGEDIN_USER_TOKEN, userToken);

        mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS).addListenerForSingleValueEvent(firebaseDBHelper.AddListenerForSingleValueEvent(AppConstants.TAG_LOGINFRAGMENT_ADDLISTENER_FORSINGLEVALUEEVENT));
        firebaseDBHelper.ValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS).child(id), AppConstants.TAG_LOGINFRAGMENT_LOGGEDIN_USER_UPDATE);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){
        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_LOGINFRAGMENT_ADDLISTENER_FORSINGLEVALUEEVENT)){
            Map<String, Object> dbEntries = (Map<String, Object>)((DataSnapshot) eventBusVO.getData()).getValue();

            if(dbEntries == null){
                createNewAccount(map);
            }

            if(map != null && !dbEntries.containsKey(map.get(AppConstants.LOGGEDIN_USER_ID))){
                createNewAccount(map);
            }

            updateToken(map);
//            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.rl_container, ChooseCategoryFragment.newInstance(argument), null).commit();
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_LOGINFRAGMENT_UPDATE_TOKEN) || eventBusVO.getTag().equals(AppConstants.TAG_LOGINFRAGMENT_LOGGEDIN_USER_UPDATE)){
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.rl_container, ChooseCategoryFragment.newInstance(argument), null).commit();
        }
    }

    public static void createNewAccount(HashMap<String, String> paramMaps){
        firebaseDBHelper.WriteInDB(paramMaps, mDatabaseReference, AppConstants.TAG_LOGINFRAGMENT_CREATENEWACCOUNT);
    }

    public static void updateToken(HashMap<String, String> paramMaps){
        String newToken = FirebaseInstanceId.getInstance().getToken();
        argument.putString(AppConstants.LOGGEDIN_USER_TOKEN, newToken);
        paramMaps.put(AppConstants.LOGGEDIN_USER_TOKEN, newToken);

        firebaseDBHelper.WriteInDB(paramMaps, mDatabaseReference, AppConstants.TAG_LOGINFRAGMENT_UPDATE_TOKEN);
    }
}
