package com.example.ziffi.locationfinderfirebase.Utils;

import android.util.Log;

import com.example.ziffi.locationfinderfirebase.Fragment.AddCommentsFragment;
import com.example.ziffi.locationfinderfirebase.Fragment.WelcomeFragment;
import com.example.ziffi.locationfinderfirebase.Model.Comment;
import com.example.ziffi.locationfinderfirebase.Model.ConnectionStatus;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.FriendData;
import com.example.ziffi.locationfinderfirebase.Model.Post;
import com.example.ziffi.locationfinderfirebase.Model.User;
import com.example.ziffi.locationfinderfirebase.Model.UserData;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import static com.example.ziffi.locationfinderfirebase.Fragment.WelcomeFragment.POST_BODY;
import static com.example.ziffi.locationfinderfirebase.Fragment.WelcomeFragment.POST_TITLE;
import static com.example.ziffi.locationfinderfirebase.Fragment.WelcomeFragment.USER_NAME;

/**
 * Created by ziffi on 11/4/16.
 */

public class FirebaseDBHelper {
    public static final String TAG = FirebaseDBHelper.class.getSimpleName();
    ValueEventListener valueEventListener;
    ChildEventListener childEventListener;


    public void ValueEventListener(DatabaseReference mReference, final String tag) {
        if(valueEventListener == null){
            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    EventBusVO eventBusVO = new EventBusVO(tag, dataSnapshot);
                    EventBus.getDefault().post(eventBusVO);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                }
            };
            mReference.addValueEventListener(valueEventListener);
        }

    }

    public void ChildEventListener (DatabaseReference mReference, final String tag){
        if (childEventListener == null){
            childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    EventBusVO eventBusVO = new EventBusVO("child_added_"+tag, dataSnapshot);
                    EventBus.getDefault().post(eventBusVO);
                    Log.e("reached", "Add");
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    EventBusVO eventBusVO = new EventBusVO("child_changed_"+tag, dataSnapshot);
                    EventBus.getDefault().post(eventBusVO);
                    Log.e("reached", "Changed");
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    EventBusVO eventBusVO = new EventBusVO("child_removed_"+tag, dataSnapshot);
                    EventBus.getDefault().post(eventBusVO);
                    Log.e("reached", "Removed");
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    EventBusVO eventBusVO = new EventBusVO("child_moved_"+tag, dataSnapshot);
                    EventBus.getDefault().post(eventBusVO);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    EventBusVO eventBusVO = new EventBusVO("child_cancelled_"+tag, databaseError);
                    EventBus.getDefault().post(eventBusVO);
                }
            };

            mReference.addChildEventListener(childEventListener);
        }
    }

    public void removeValueEventListener(DatabaseReference mReference) {
        if (mReference != null && valueEventListener != null)
            mReference.removeEventListener(valueEventListener);
    }

    public ValueEventListener AddListenerForSingleValueEvent(final String tag) {
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EventBusVO eventBusVO = new EventBusVO(tag, dataSnapshot);
                EventBus.getDefault().post(eventBusVO);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };

        return eventListener;

    }



    public void WriteInDB(HashMap<String, String> paramMaps, DatabaseReference mReference, String tag) {
        if (tag.equals(WelcomeFragment.WRITE_USER)) {
            String tableName = paramMaps.get(WelcomeFragment.TABLE_USERS);
            String username = paramMaps.get(USER_NAME);
            String email = paramMaps.get(WelcomeFragment.USER_EMAIL);
            String userId = paramMaps.get(WelcomeFragment.USER_ID);

            User user = new User(username, email);
            mReference.child(tableName).child(userId).setValue(user);
        }

        if (tag.equals(WelcomeFragment.WRITE_DB)) {
            String userid = paramMaps.get(WelcomeFragment.USER_ID);
            String username = paramMaps.get(USER_NAME);
            String posttitle = paramMaps.get(POST_TITLE);
            String postbody = paramMaps.get(POST_BODY);

            String key = mReference.child(FirebaseDBAttributes.TABLE_POSTS).push().getKey();
            Post post = new Post(userid, username, posttitle, postbody);
            Map<String, Object> postValues = post.toMap();
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.put("/" + FirebaseDBAttributes.TABLE_POSTS + "/" + key, postValues);
            childUpdates.put("/" + FirebaseDBAttributes.TABLE_USERS_POSTS + "/" + userid + "/" + key, postValues);

            mReference.updateChildren(childUpdates);
        }

        if (tag.equals(AddCommentsFragment.WRITE_COMMENTS)) {
            String tableName = paramMaps.get(AddCommentsFragment.TABLE_COMMENTS);
            String postKey = paramMaps.get(AddCommentsFragment.POST_KEY);
            String commentText = paramMaps.get(AddCommentsFragment.COMMENT_TEXT);
            Comment comment = new Comment("ABCD", commentText);
            mReference.child(tableName).child(postKey).setValue(comment);
        }

        if(tag.equals(AppConstants.TAG_LOGINFRAGMENT_CREATENEWACCOUNT)){
            String id = paramMaps.get(AppConstants.LOGGEDIN_USER_ID);
            String name = paramMaps.get(AppConstants.LOGGEDIN_USER_NAME);
            String email = paramMaps.get(AppConstants.LOGGEDIN_USER_EMAIL);
            String profilepic = paramMaps.get(AppConstants.LOGGEDIN_USER_PROFILE_PIC);
            String token = paramMaps.get(AppConstants.LOGGEDIN_USER_TOKEN);

            UserData userData = new UserData(id, name, email, profilepic, token);
            mReference.child(FirebaseDBAttributes.TABLE_USERS).child(id).setValue(userData);
        }

        if(tag.equals(AppConstants.TAG_LOGINFRAGMENT_UPDATE_TOKEN)){
            String id = paramMaps.get(AppConstants.LOGGEDIN_USER_ID);
            String name = paramMaps.get(AppConstants.LOGGEDIN_USER_NAME);
            String email = paramMaps.get(AppConstants.LOGGEDIN_USER_EMAIL);
            String profilepic = paramMaps.get(AppConstants.LOGGEDIN_USER_PROFILE_PIC);
            String token = paramMaps.get(AppConstants.LOGGEDIN_USER_TOKEN);

            UserData userData = new UserData(id, name, email, profilepic, token);
            mReference.child(FirebaseDBAttributes.TABLE_USERS).child(id).child("userToken").setValue(token);
        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            ConnectionStatus connectionStatusSender = new ConnectionStatus(requestReceiverId, AppConstants.SENT_REQUEST);
            ConnectionStatus connectionStatusReceiver = new ConnectionStatus(requestSenderId, AppConstants.RECEIVED_REQUEST);

//            Map<String, Object> childUpdates = new HashMap<>();
////            String key = mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).push().getKey();
//            childUpdates.put(requestReceiverId, connectionStatus);
//
//            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).updateChildren(childUpdates);


            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).setValue(connectionStatusSender);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestReceiverId).child(requestSenderId).setValue(connectionStatusReceiver);
        }

//        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_RECEIVED)){
//            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
//            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);
//
//            ConnectionStatus connectionStatusSender = new ConnectionStatus(requestReceiverId, AppConstants.SENT_REQUEST);
//            ConnectionStatus connectionStatusReceiver = new ConnectionStatus(requestSenderId, AppConstants.RECEIVED_REQUEST);
//
////            Map<String, Object> childUpdates = new HashMap<>();
//////            String key = mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).push().getKey();
////            childUpdates.put(requestReceiverId, connectionStatus);
////
////            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).updateChildren(childUpdates);
//
//
////            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).setValue(connectionStatusSender);
//            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestReceiverId).child(requestSenderId).setValue(connectionStatusReceiver);
//        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_CANCEL)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            ConnectionStatus connectionStatusSender = new ConnectionStatus(requestReceiverId, AppConstants.REQUEST_TO_CONNECT);
            ConnectionStatus connectionStatusReceiver = new ConnectionStatus(requestSenderId, AppConstants.REQUEST_TO_CONNECT);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).setValue(connectionStatusSender);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestReceiverId).child(requestSenderId).setValue(connectionStatusReceiver);
        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_ACCEPTED)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            ConnectionStatus connectionStatusSender = new ConnectionStatus(requestReceiverId, AppConstants.FRIEND);
            ConnectionStatus connectionStatusReceiver = new ConnectionStatus(requestSenderId, AppConstants.FRIEND);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).setValue(connectionStatusSender);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestReceiverId).child(requestSenderId).setValue(connectionStatusReceiver);
        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_CLICKED)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            ConnectionStatus connectionStatusSender = new ConnectionStatus(requestReceiverId, AppConstants.REQUEST_TO_CONNECT);
            ConnectionStatus connectionStatusReceiver = new ConnectionStatus(requestSenderId, AppConstants.REQUEST_TO_CONNECT);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestSenderId).child(requestReceiverId).setValue(connectionStatusSender);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(requestReceiverId).child(requestSenderId).setValue(connectionStatusReceiver);
        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_ADD_FRIEND)){
            String userId = paramMaps.get(AppConstants.LOGGEDIN_USER_ID);
            String userName = paramMaps.get(AppConstants.LOGGEDIN_USER_NAME);
            String userEmail = paramMaps.get(AppConstants.LOGGEDIN_USER_EMAIL);
            String userProfile = paramMaps.get(AppConstants.LOGGEDIN_USER_PROFILE_PIC);
            String userToken = paramMaps.get(AppConstants.LOGGEDIN_USER_TOKEN);

            String userFriendId = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_ID);
            String userFriendName = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_NAME);
            String userFriendEmail = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_EMAIL);
            String userFriendProfile = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_PROFILE_PIC);
            String userFriendToken = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_TOKEN);
            int friendConnectionStatus = AppConstants.FRIEND;

            FriendData myData = new FriendData(userId, userName, userEmail, userProfile, userToken, friendConnectionStatus);
            FriendData myFriendData = new FriendData(userFriendId, userFriendName, userFriendEmail, userFriendProfile, userFriendToken, friendConnectionStatus);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userId).child(userFriendId).setValue(myFriendData);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userFriendId).child(userId).setValue(myData);
        }

        if(tag.equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_REMOVE_FRIEND)){
            String userId = paramMaps.get(AppConstants.LOGGEDIN_USER_ID);
            String userFriendId = paramMaps.get(AppConstants.LOGGEDIN_USER_FRIEND_ID);

            if(mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userId).child(userFriendId) != null){
                mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userId).child(userFriendId).removeValue();
            }

            if(mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userFriendId).child(userId) != null){
                mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(userFriendId).child(userId).removeValue();
            }

        }

        if(tag.equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_SEND_TRACKING_REQUEST)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            if(AppUtils.isStringValid(requestReceiverId) && AppUtils.isStringValid(requestSenderId)){
                mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestSenderId).child(requestReceiverId).child("friendConnectionStatus").setValue(AppConstants.TRACK_REQUEST_SENT);
                mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestReceiverId).child(requestSenderId).child("friendConnectionStatus").setValue(AppConstants.TRACK_ACCEPT_REQUEST);
            }

        }

        if(tag.equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestSenderId).child(requestReceiverId).child("friendConnectionStatus").setValue(AppConstants.FRIEND);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestReceiverId).child(requestSenderId).child("friendConnectionStatus").setValue(AppConstants.FRIEND);
        }

        if(tag.equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_ACCEPTED_TRACKING_REQUEST)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestSenderId).child(requestReceiverId).child("friendConnectionStatus").setValue(AppConstants.TRACK_FRIEND);
            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestReceiverId).child(requestSenderId).child("friendConnectionStatus").setValue(AppConstants.TRACK_FRIEND);
        }

        if(tag.equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_TRACKING_PAUSED)){
            String requestSenderId = paramMaps.get(AppConstants.REQUEST_SENDER_ID);
            String requestReceiverId = paramMaps.get(AppConstants.REQUEST_RECEIVER_ID);

            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestSenderId).child(requestReceiverId).child("friendConnectionStatus").setValue(AppConstants.TRACK_PAUSED);
//            mReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(requestReceiverId).child(requestSenderId).child("friendConnectionStatus").setValue(AppConstants.TRACK_PAUSED);
        }

    }

    public void addUserConnection(String userId, FriendData friendData){
        if(userId != null && friendData.getFriendId() != null){
            FirebaseDatabase.getInstance().getReference().child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(userId).child(friendData.getFriendId()).setValue(friendData);
        }
    }

    public void fetchUserConnection(){

    }




}
