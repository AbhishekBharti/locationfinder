package com.example.ziffi.locationfinderfirebase.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Fragment.BaseFragment;
import com.example.ziffi.locationfinderfirebase.Model.UserData;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Widget.RoundedImageView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.Executor;

/**
 * Created by ziffi on 11/3/16.
 */

public class LoginHelper {
    public final String TAG = LoginHelper.class.getSimpleName();

    LoginButton mLoginButton;
    TextView tvLoginGmail;
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Context mContext;
    private LoginData mLoginData;
    private AccessToken accessToken;
    String userId, userName, userEmail, userDob, profilePicUri, userToken;
    UserData userData;

    public interface LoginData{
        void setLoginData(String id, String name, String email, String profilepic, String userToken);
    }

    public void FacebookLogin(Context context, LoginButton loginButton, CallbackManager callbackManager, FirebaseAuth auth, FirebaseAuth.AuthStateListener authListener, LoginData loginData){
        this.mContext = context;
        this.mLoginButton = loginButton;
        this.mCallbackManager = callbackManager;
        this.mAuth = auth;
        this.mAuthListener = authListener;
        this.mLoginData = loginData;


        mLoginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);

                ((BaseActivity) mContext).showLoaderEffect();

                accessToken = loginResult.getAccessToken();
                handleFacebookAccessToken(accessToken);
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid() + "Name : "+ user.getDisplayName() + " Email : "+user.getEmail());

//                    GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
//                        @Override
//                        public void onCompleted(JSONObject object, GraphResponse response) {
//                            try {
//                                friendId = object.getString("birthday");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//
//                    Bundle parameters = new Bundle();
//                    parameters.putString("fields", "id,name,email,gender,birthday");
//                    request.setParameters(parameters);
//                    request.executeAsync();

                    userId = user.getUid();
                    userName = user.getDisplayName();
                    userEmail = user.getEmail();
                    profilePicUri = user.getPhotoUrl().toString();
                    userToken = FirebaseInstanceId.getInstance().getToken();

                    userData = new UserData(userId, userName, userEmail, profilePicUri, userToken);


//                    new GraphRequest(
//                            accessToken,
//                            "/{user-id}/friends",
//                            null,
//                            HttpMethod.GET,
//                            new GraphRequest.Callback() {
//                                public void onCompleted(GraphResponse response) {
//            /* handle the result */
//                                    Log.d("fb_friends_response :", response.toString());
//
//                                }
//                            }
//                    ).executeAsync();




                    mLoginData.setLoginData(userId, userName, userEmail, profilePicUri, userToken);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
//                            Toast.makeText(MainActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    public void AddAuthStateListener(){
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void RemoveAuthStateListener(){
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

//    public Bitmap getFacebookProfilePicture(String userID){
//        URL imageURL = null;
//        try {
//            imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        Bitmap bitmap = null;
//        try {
//            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return bitmap;
//    }

    public void setImageFromLink(RoundedImageView userpicture, Context context, String profilePicUri){
        Picasso.with(context)
                .load(profilePicUri)
                .into(userpicture);
    }


}
