package com.example.ziffi.locationfinderfirebase.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.NewComment;
import com.example.ziffi.locationfinderfirebase.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ziffi on 11/7/16.
 */

public class NewCommentAdapter extends RecyclerView.Adapter<NewCommentAdapter.CommentViewHolder> {

    private Context mContext;
    private DatabaseReference mDatabaseReference;
    private ChildEventListener mChildEventListener;

    private List<String> mCommentIds = new ArrayList<>();
    private List<NewComment> mComments = new ArrayList<>();

    public NewCommentAdapter(final Context mContext, DatabaseReference mDatabaseReference) {
        this.mContext = mContext;
        this.mDatabaseReference = mDatabaseReference;

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                NewComment newComment = dataSnapshot.getValue(NewComment.class);
                mCommentIds.add(dataSnapshot.getKey());
                mComments.add(newComment);
                notifyItemInserted(mComments.size()-1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                NewComment newComment = dataSnapshot.getValue(NewComment.class);
                String key = dataSnapshot.getKey();
                int commentIndex = mCommentIds.indexOf(key);
                if(commentIndex > -1){
                    mComments.set(commentIndex, newComment);
                    notifyItemChanged(commentIndex);
                } else {
                    Log.w("newCommentAdapter", "onChildChanged:unknown_child:" + key);
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                int commentIndex = mCommentIds.indexOf(key);
                if(commentIndex > -1){
                    mCommentIds.remove(commentIndex);
                    mComments.remove(commentIndex);
                    notifyItemRemoved(commentIndex);

                }else {
                    Log.w("newCommentAdapter", "onChildRemoved:unknown_child:" + key);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                NewComment newComment = dataSnapshot.getValue(NewComment.class);
                String key = dataSnapshot.getKey();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabaseReference.addChildEventListener(childEventListener);
    }

    @Override
    public NewCommentAdapter.CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.post_comment_item, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewCommentAdapter.CommentViewHolder holder, int position) {
        NewComment newComment = mComments.get(position);
        holder.tvAuthorView.setText(newComment.authorName);
        holder.tvBodyView.setText(newComment.commentText);
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        TextView tvAuthorView, tvBodyView;
        public CommentViewHolder(View itemView) {
            super(itemView);
            tvAuthorView = (TextView) itemView.findViewById(R.id.tv_commentor_name);
            tvBodyView = (TextView) itemView.findViewById(R.id.tv_commentor_comment);
        }
    }

    public void cleanupListener(){
        if(mChildEventListener != null){
            mDatabaseReference.removeEventListener(mChildEventListener);
        }
    }
}
