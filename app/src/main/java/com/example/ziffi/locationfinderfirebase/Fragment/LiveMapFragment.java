package com.example.ziffi.locationfinderfirebase.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ziffi.locationfinderfirebase.R;

/**
 * Created by ziffi on 11/27/16.
 */

public class LiveMapFragment extends BaseFragment {

    public static LiveMapFragment newInstance(Bundle args){
        LiveMapFragment fragment = new LiveMapFragment();
        if(args != null){
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.live_map_fragment, container, false);

        return view;
    }
}
