package com.example.ziffi.locationfinderfirebase.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.Comment;
import com.example.ziffi.locationfinderfirebase.R;

import java.util.ArrayList;


/**
 * Created by ziffi on 11/7/16.
 */

public class AddCommentsAdapter extends RecyclerView.Adapter<AddCommentsAdapter.ViewHolder> {

    ArrayList<Comment> commentArrayList = new ArrayList<>();
    Context context;

    public AddCommentsAdapter(ArrayList<Comment> commentArrayList, Context context) {
        this.commentArrayList = commentArrayList;
        this.context = context;
    }

    @Override
    public AddCommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_comment_item, null);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddCommentsAdapter.ViewHolder holder, int position) {
        holder.tvCommentorName.setText(commentArrayList.get(position).getUserName());
        holder.tvCommentorComment.setText(commentArrayList.get(position).getCommentText());
    }

    @Override
    public int getItemCount() {
        return commentArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCommentorName, tvCommentorComment;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCommentorName = (TextView) itemView.findViewById(R.id.tv_commentor_name);
            tvCommentorComment = (TextView) itemView.findViewById(R.id.tv_commentor_comment);
        }
    }
}
