package com.example.ziffi.locationfinderfirebase.Network;

import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Model.NotificationResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by ziffi on 11/16/16.
 */

public interface ApiInterface {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AIzaSyAsxvcOdUIWcA0-Ss-qXQniG8VN5z7j8RQ"
    })

    @POST("fcm/send")
    Call<NotificationResponse> getNotificationResponse(@Body NotificationInput notificationInput);

}
