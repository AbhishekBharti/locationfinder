package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/14/16.
 */

public class FriendData {
    public String friendId;
    public String friendName;
    public String friendEmail;
    public String friendProfilePic;
    public String friendtoken;
    public int friendConnectionStatus = 0;

    public FriendData(){}

    public FriendData(String friendId, String friendName, String friendEmail, String friendProfilePic, String friendtoken, int friendConnectionStatus) {
        this.friendId = friendId;
        this.friendName = friendName;
        this.friendEmail = friendEmail;
        this.friendProfilePic = friendProfilePic;
        this.friendtoken = friendtoken;
        this.friendConnectionStatus = friendConnectionStatus;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendEmail() {
        return friendEmail;
    }

    public void setFriendEmail(String friendEmail) {
        this.friendEmail = friendEmail;
    }

    public String getFriendProfilePic() {
        return friendProfilePic;
    }

    public void setFriendProfilePic(String friendProfilePic) {
        this.friendProfilePic = friendProfilePic;
    }

    public String getFriendtoken() {
        return friendtoken;
    }

    public void setFriendtoken(String friendtoken) {
        this.friendtoken = friendtoken;
    }

    public int getFriendConnectionStatus() {
        return friendConnectionStatus;
    }

    public void setFriendConnectionStatus(int friendConnectionStatus) {
        this.friendConnectionStatus = friendConnectionStatus;
    }
}
