package com.example.ziffi.locationfinderfirebase.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.Post;
import com.example.ziffi.locationfinderfirebase.Model.User;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppUtils;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ziffi on 11/4/16.
 */

public class WelcomeFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = WelcomeFragment.class.getSimpleName();
    public static final String USER_REFERENCE = "user_ref";
    public static final String WELCOME_FRAGMENT_DATABASE_REFERENCE = "welcome_fragment_database_reference";
//    public static final String LOGGED_IN_USER_NAME = "logged_in_user_name";
    public static final String SHAREDPREF_UID = "sharedpref_uid";
    public static final String TABLE_USERS = "table_users";
    public static final String WRITE_USER = "write_user";
    public static final String WRITE_DB = "write_db";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String POST_TITLE = "post_title";
    public static final String POST_BODY = "post_body";


    private TextView tvUserName, tvUserInfo, tvPostDetails;
    private EditText etUserName, etEmail, etTitle, etBody;
    private Button btnAdd, btnSubmit, btnComments, btnMap;
    String UserName;
    int userId = 0;
    RelativeLayout rlWelcomeScreenContainer;
    private DatabaseReference mDatabaseReference;
    SharedPreferences sharedPreferences;
    DatabaseReference mUserReference;
    String mUserKey;
    FirebaseDBHelper firebaseDbHelper = new FirebaseDBHelper();
    HashMap<String, String> paramMaps;

    public static WelcomeFragment newInstance(Bundle args){
        WelcomeFragment welcomeFragment = new WelcomeFragment();
        if(args != null){
            welcomeFragment.setArguments(args);
        }
        return welcomeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        View view = inflater.inflate(R.layout.welcome_fragment, container, false);
        init(view);
        btnAdd.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnComments.setOnClickListener(this);
        btnMap.setOnClickListener(this);

        sharedPreferences = getActivity().getSharedPreferences(SHAREDPREF_UID, Context.MODE_PRIVATE);
        UserName = getArguments().getString(AppConstants.LOGGEDIN_USER_NAME);
        tvUserName.setText(UserName);

        return view;
    }

    public void init(View view){
        rlWelcomeScreenContainer = (RelativeLayout) view.findViewById(R.id.rl_welcome_screen_container);
        tvUserName = (TextView) view.findViewById(R.id.tv_user_name);
        tvUserInfo = (TextView) view.findViewById(R.id.tv_user_info);
        etUserName = (EditText) view.findViewById(R.id.et_username);
        etEmail = (EditText) view.findViewById(R.id.et_email);
        tvPostDetails = (TextView) view.findViewById(R.id.tv_post_details);
        etTitle = (EditText) view.findViewById(R.id.et_title);
        etBody = (EditText) view.findViewById(R.id.et_body);
        btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        btnAdd = (Button) view.findViewById(R.id.btn_add);
        btnComments = (Button) view.findViewById(R.id.btn_comments);
        btnMap = (Button) view.findViewById(R.id.btn_map);
    }

    @Override
    public void onStart() {
        super.onStart();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mUserReference = FirebaseDatabase.getInstance().getReference().child(FirebaseDBAttributes.TABLE_USERS).child("1");

        firebaseDbHelper.ValueEventListener(mUserReference, USER_REFERENCE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add:
                if(AppUtils.isStringValid(etUserName.getText().toString()) && AppUtils.isStringValid(etEmail.getText().toString())){

                    userId = 1 + sharedPreferences.getInt(SHAREDPREF_UID, 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt(SHAREDPREF_UID, userId);
                    editor.commit();

                    String id = userId +"";

                    paramMaps = new HashMap<>();
                    paramMaps.put(TABLE_USERS, FirebaseDBAttributes.TABLE_USERS);
                    paramMaps.put(USER_ID, id);
                    paramMaps.put(USER_NAME, etUserName.getText().toString());
                    paramMaps.put(USER_EMAIL, etEmail.getText().toString());

                    firebaseDbHelper.WriteInDB(paramMaps, mDatabaseReference, WRITE_USER);

                    etUserName.setText("");
                    etEmail.setText("");
                    Toast.makeText(getActivity(), "User Added!", Toast.LENGTH_SHORT).show();

                } else {
                    Snackbar snackbar = Snackbar
                            .make(rlWelcomeScreenContainer, "Please fill both the field!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                break;

            case R.id.btn_submit:
                if(AppUtils.isStringValid(etTitle.getText().toString()) && AppUtils.isStringValid(etBody.getText().toString())){

                    setEditingEnable(false);
                    Toast.makeText(getActivity(), "Posting...", Toast.LENGTH_SHORT).show();

                    ValueEventListener valueEventListener = firebaseDbHelper.AddListenerForSingleValueEvent(WELCOME_FRAGMENT_DATABASE_REFERENCE);
                    String tableName = paramMaps.get(WelcomeFragment.TABLE_USERS);
                    String userId = paramMaps.get(WelcomeFragment.USER_ID);
                    mDatabaseReference.child(tableName).child(userId).addListenerForSingleValueEvent(valueEventListener);

                } else {
                    Snackbar snackbar = Snackbar
                            .make(rlWelcomeScreenContainer, "Please fill both the field!", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }

                break;

            case R.id.btn_comments:
                fragTansaction(WelcomeFragment.newInstance(null), AddCommentsFragment.newInstance(null), "welcome-comment");
                break;

            case R.id.btn_map:
                fragTansaction(WelcomeFragment.newInstance(null), MapFragment.newInstance(null), "welcome-map");
                break;
        }
    }

//    private void submitPost(){
//        final String title = etTitle.getText().toString();
//        final String body = etBody.getText().toString();
//
//        //Title is Required
//        if(title == null || title.equals("")){
//            etTitle.setError("required");
//            return;
//        }
//        //body is required
//        if(body == null || body.equals("")){
//            etBody.setError("required");
//            return;
//        }
//        //disable button to avoid multipost
//        setEditingEnable(false);
//        Toast.makeText(getActivity(), "Posting...", Toast.LENGTH_SHORT).show();
//
////        final String friendId = getUid();
//        final String friendId = "1";
////        mDatabaseReference.child("Users").child("2").removeValue(); // to delete value
////        mDatabaseReference.child("Users").child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
////            @Override
////            public void onDataChange(DataSnapshot dataSnapshot) {
////                //Get User value
////                User user = dataSnapshot.getValue(User.class);
////
////                if(user == null){
////                    Log.e(TAG, "User " + friendId + " is unexpectedly null");
////                    Toast.makeText(getActivity(), "Error: could not fetch user.", Toast.LENGTH_SHORT).show();
////                } else {
////                    WriteNewPost(friendId, user.username.toString(), title, body);
////                }
////
////                setEditingEnable(true);
////            }
////
////            @Override
////            public void onCancelled(DatabaseError databaseError) {
////                Log.w(TAG, "getUser:onCancelled", databaseError.toException());
////                setEditingEnable(true);
////            }
////        });
//        paramMaps = new HashMap<>();
//        paramMaps.put(TABLE_USERS, "Users");
//        paramMaps.put(USER_ID, friendId);
//
//        firebaseDbHelper.AddListenerForSingleValueEvent(paramMaps, mDatabaseReference, WELCOME_FRAGMENT_DATABASE_REFERENCE);
//    }

    private void WriteNewPost(String uid, String username, String title, String body){
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabaseReference.child("Posts").push().getKey();
        Post post = new Post(uid, username, title, body);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/Posts/" + key, postValues);
        childUpdates.put("/Users-Posts/" + uid + "/" + key, postValues);

        mDatabaseReference.updateChildren(childUpdates);
    }

    private void setEditingEnable(boolean enable){
        etTitle.setEnabled(enable);
        etBody.setEnabled(enable);
        if(enable){
            btnSubmit.setVisibility(View.VISIBLE);
        } else {
            btnSubmit.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVo){
        if(eventBusVo != null && eventBusVo.getTag().equals(USER_REFERENCE)){
            User user =  ((DataSnapshot) eventBusVo.getData()).getValue(User.class);
            if(user != null){
                tvUserInfo.setText(user.username.toString() +"\n"+ user.email.toString() +"\n");
            } else{
                tvUserInfo.setText("");
            }
        }

        if(eventBusVo != null && eventBusVo.getTag().equals(WELCOME_FRAGMENT_DATABASE_REFERENCE)){
            User user = ((DataSnapshot) eventBusVo.getData()).getValue(User.class);

            if(user == null){
                Log.e(TAG, "User " + userId + " is unexpectedly null");
                Toast.makeText(getActivity(), "Error: could not fetch user.", Toast.LENGTH_SHORT).show();
            } else {
                final String title = etTitle.getText().toString();
                final String body = etBody.getText().toString();

                paramMaps = new HashMap<>();
                paramMaps.put(USER_ID, "1");
                paramMaps.put(USER_NAME, user.username.toString());
                paramMaps.put(POST_TITLE, title);
                paramMaps.put(POST_BODY, body);

                firebaseDbHelper.WriteInDB(paramMaps, mDatabaseReference, WRITE_DB);
            }

            setEditingEnable(true);
        }
    }
}
