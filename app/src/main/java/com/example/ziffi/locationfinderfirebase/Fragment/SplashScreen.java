package com.example.ziffi.locationfinderfirebase.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.LocationFinderApplication;

/**
 * Created by ziffi on 11/3/16.
 */

public class SplashScreen extends BaseFragment {
    public static final String TAG = SplashScreen.class.getSimpleName();
    ImageView ivSplashCentralImage;
    TextView tvWelcome;
    Bundle argument = new Bundle();

    public static SplashScreen newInstance (){
        SplashScreen splashScreen = new SplashScreen();
        return splashScreen;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View view = inflater.inflate(R.layout.splash_screen, container, false);
        ivSplashCentralImage = (ImageView) view.findViewById(R.id.iv_splash_central_image);
        tvWelcome = (TextView) view.findViewById(R.id.tv_welcome);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lost in Wild.ttf");
        tvWelcome.setTypeface(typeface);
        tvWelcome.setVisibility(View.GONE);

        startRiseupAnimation();
        startZoomAnimation();

        return view;
    }

    public void startRiseupAnimation(){
        final Animation riseUp = AnimationUtils.loadAnimation(getActivity(), R.anim.riseup);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tvWelcome.setVisibility(View.VISIBLE);
                tvWelcome.setAnimation(riseUp);
            }
        }, 800);
    }

    public void startZoomAnimation(){
        Animation zoomin = AnimationUtils.loadAnimation(getActivity(), R.anim.zoomin);
        ivSplashCentralImage.setAnimation(zoomin);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(LocationFinderApplication.getBooleanSharedPreference(AppConstants.IS_USER_LOGGEDIN) && argument != null){
                    String loggedin_userid = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                    String loggedin_username = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);
                    String loggedin_useremail = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_EMAIL);
                    String loggedin_userpic = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_PROFILE_PIC);

                    argument.putString(AppConstants.LOGGEDIN_USER_ID, loggedin_userid);
                    argument.putString(AppConstants.LOGGEDIN_USER_NAME, loggedin_username);
                    argument.putString(AppConstants.LOGGEDIN_USER_EMAIL, loggedin_useremail);
                    argument.putString(AppConstants.LOGGEDIN_USER_PROFILE_PIC, loggedin_userpic);

                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.rl_container, ChooseCategoryFragment.newInstance(argument), null).commit();
                } else{
                    getFragmentManager().beginTransaction().replace(R.id.rl_container, LoginFragment.newInstance(null), LoginFragment.TAG).commit();
                }


            }
        }, 2000);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((BaseActivity) getActivity()).hideActionBar();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((BaseActivity) getActivity()).showActionBar();
    }
}
