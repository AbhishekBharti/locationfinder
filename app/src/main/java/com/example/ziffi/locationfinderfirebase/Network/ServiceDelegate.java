package com.example.ziffi.locationfinderfirebase.Network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Model.NotificationResponse;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ziffi on 11/20/16.
 */

public class ServiceDelegate {

    private static ServiceDelegate instance;
    private Context mContext;

    public static ServiceDelegate getInstance(){
        if(instance == null){
            instance = new ServiceDelegate();
        }
        return instance;
    }

    public void getNotificatioResponse(NotificationInput notificationInput, Context context, final String tag){
        this.mContext = context;

        Call<NotificationResponse> call = NetworkDAO.getInstance().getNotificatioResponse(notificationInput);

        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                if (response.code() == 400 ) {
//                    Log.d(TAG, "onResponse - Status : " + response.code());
                    try {
                        if (response.errorBody() != null){
                            Log.e("error 400 :",response.errorBody().string());

                            EventBusVO eventBusVO = new EventBusVO(tag +"Response", response);
                            EventBus.getDefault().post(eventBusVO);
//                            Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if(response.body() != null){
                    response.body().toString();
                    EventBusVO eventBusVO = new EventBusVO(tag+"Response", response);
                    EventBus.getDefault().post(eventBusVO);

//                    HashMap<String, String> paramMaps = new HashMap<String, String>();
//                    paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
//                    paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
//                    DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
//
//                    firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT);

                } else {
                    Log.e("Error: ", "Something went wrong!!!");
                }


                Log.e("Call ", call.toString());
                Log.e("Response ", response.toString());
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                // Log error here since request failed
//                Log.e(TAG, t.toString());
                Log.e("Response ", "Failed");
                EventBusVO eventBusVO = new EventBusVO(tag+"Error", t);
                EventBus.getDefault().post(eventBusVO);
            }
        });
    }
}
