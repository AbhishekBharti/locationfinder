package com.example.ziffi.locationfinderfirebase.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.FriendData;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Widget.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ziffi on 11/22/16.
 */

public class TrackRequestAdapter extends RecyclerView.Adapter<TrackRequestAdapter.ViewHolder> {

    public interface TrackRequestItemClickListener{
        void requestClicked(FriendData friendData, int position, TextView textView);
    }

    TrackRequestItemClickListener trackRequestItemClickListener;
    Context mContext;
    ArrayList<FriendData> selectedFriendDataArrayList;

    public TrackRequestAdapter(ArrayList<FriendData> selectedFriendDataArrayList, Context mContext, TrackRequestItemClickListener trackRequestItemClickListener) {
        this.trackRequestItemClickListener = trackRequestItemClickListener;
        this.mContext = mContext;
        this.selectedFriendDataArrayList = selectedFriendDataArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.track_request_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(mContext).load(selectedFriendDataArrayList.get(position).getFriendProfilePic()).into(holder.rivUserImage);
        holder.tvUserName.setText(selectedFriendDataArrayList.get(position).getFriendName());
        int currentStatus = selectedFriendDataArrayList.get(position).getFriendConnectionStatus();
        if(currentStatus == AppConstants.TRACK_REQUEST_SENT){
            holder.tvRequestStatus.setText(AppConstants.BTN_REQUESTED);
            holder.tvRequestStatus.setBackgroundResource(R.drawable.round_corner_requested_bg);
            holder.tvRequestStatus.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvRequestStatus.setElevation(5);
            }
        } else if(currentStatus == AppConstants.TRACK_FRIEND){
            holder.tvRequestStatus.setText(AppConstants.BTN_CONNECTED);
            holder.tvRequestStatus.setBackgroundResource(R.drawable.round_corner_friend_bg);
            holder.tvRequestStatus.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvRequestStatus.setElevation(5);
            }
        } else if(currentStatus == AppConstants.TRACK_ACCEPT_REQUEST){
            holder.tvRequestStatus.setText(AppConstants.BTN_ACCEPT_REJECT);
            holder.tvRequestStatus.setBackgroundResource(R.drawable.round_corner_accept_bg);
            holder.tvRequestStatus.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvRequestStatus.setElevation(5);
            }
        } else if(currentStatus == AppConstants.TRACK_PAUSED){
            holder.tvRequestStatus.setText(AppConstants.BTN_PAUSED);
            holder.tvRequestStatus.setBackgroundResource(R.drawable.round_corner_accept_bg);
            holder.tvRequestStatus.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvRequestStatus.setElevation(5);
            }
        } else {
            holder.tvRequestStatus.setText(AppConstants.BTN_CONNECT);
            holder.tvRequestStatus.setBackgroundResource(R.drawable.round_corner_connect_bg);
            holder.tvRequestStatus.setPadding(40, 15, 40, 15);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.tvRequestStatus.setElevation(5);
            }
        }


    }

    @Override
    public int getItemCount() {
        return selectedFriendDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RoundedImageView rivUserImage;
        TextView tvUserName, tvRequestStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            rivUserImage = (RoundedImageView) itemView.findViewById(R.id.riv_user_pic);
            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            tvRequestStatus = (TextView) itemView.findViewById(R.id.tv_connection_status_btn);

            tvRequestStatus.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            trackRequestItemClickListener.requestClicked(selectedFriendDataArrayList.get(getAdapterPosition()), getAdapterPosition(), tvRequestStatus);
        }
    }
}
