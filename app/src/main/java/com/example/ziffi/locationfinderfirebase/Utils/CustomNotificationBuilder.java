package com.example.ziffi.locationfinderfirebase.Utils;

import com.example.ziffi.locationfinderfirebase.Model.NotificationData;
import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Model.NotificationParams;

/**
 * Created by ziffi on 11/19/16.
 */

public class CustomNotificationBuilder {

    public NotificationInput connectionRequestNotification(String requestRecieverUserToken){

        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);

        String notificationBody = "";
        if(AppUtils.isStringValid(requestSenderUserName)){
            notificationBody = requestSenderUserName +" wants to connect with you";
        } else {
            notificationBody = "You got a new Connection request";
        }

        NotificationData nData = new NotificationData("5x1", "15:10");
        String to = requestRecieverUserToken;
        NotificationParams nParams = new NotificationParams(notificationBody, "Connection Request");

        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

        return notificationInput;
    }

    public NotificationInput trackingRequestNotification(String requestRecieverUserToken){

        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);

        String notificationBody = "";
        if(AppUtils.isStringValid(requestSenderUserName)){
            notificationBody = requestSenderUserName +" wants to track your location";
        } else {
            notificationBody = "You got a new tracking request";
        }

        NotificationData nData = new NotificationData("5x1", "15:10");
        String to = requestRecieverUserToken;
        NotificationParams nParams = new NotificationParams(notificationBody, "Geo-Tracking Request");

        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

        return notificationInput;
    }

    public NotificationInput acceptTrackingRequestNotification(String requestRecieverUserToken){

        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);

        String notificationBody = "";
        if(AppUtils.isStringValid(requestSenderUserName)){
            notificationBody = requestSenderUserName +" has accepted your tracking request. You can now track each others location.";
        } else {
            notificationBody = "Your tracking request has been accepted";
        }

        NotificationData nData = new NotificationData("5x1", "15:10");
        String to = requestRecieverUserToken;
        NotificationParams nParams = new NotificationParams(notificationBody, "Tracking Request Accepted");

        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

        return notificationInput;
    }

    public NotificationInput trackingPausedNotification(String requestRecieverUserToken){

        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);

        String notificationBody = "";
        if(AppUtils.isStringValid(requestSenderUserName)){
            notificationBody = requestSenderUserName +" has temporarily paused location tracking.";
        } else {
            notificationBody = "Location tracking has been paused";
        }

        NotificationData nData = new NotificationData("5x1", "15:10");
        String to = requestRecieverUserToken;
        NotificationParams nParams = new NotificationParams(notificationBody, "Tracking Paused");

        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

        return notificationInput;
    }

    public NotificationInput trackingResumedNotification(String requestRecieverUserToken){

        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);

        String notificationBody = "";
        if(AppUtils.isStringValid(requestSenderUserName)){
            notificationBody = requestSenderUserName +" has resumed location tracking.";
        } else {
            notificationBody = "Location tracking has been resumed";
        }

        NotificationData nData = new NotificationData("5x1", "15:10");
        String to = requestRecieverUserToken;
        NotificationParams nParams = new NotificationParams(notificationBody, "Tracking Resumed");

        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

        return notificationInput;
    }

}
