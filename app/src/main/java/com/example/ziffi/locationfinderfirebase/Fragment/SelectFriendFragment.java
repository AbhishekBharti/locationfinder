package com.example.ziffi.locationfinderfirebase.Fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Adapter.SelectFriendAdapter;
import com.example.ziffi.locationfinderfirebase.Adapter.TrackRequestAdapter;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.FriendData;
import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Network.ServiceDelegate;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.AppUtils;
import com.example.ziffi.locationfinderfirebase.Utils.CustomNotificationBuilder;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.example.ziffi.locationfinderfirebase.Utils.LocationFinderApplication;
import com.example.ziffi.locationfinderfirebase.Widget.RoundedImageView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ziffi on 11/13/16.
 */

public class SelectFriendFragment extends BaseFragment implements View.OnClickListener, SelectFriendAdapter.SelectFriend, TrackRequestAdapter.TrackRequestItemClickListener{
    public static final String TAG = SelectFriendFragment.class.getSimpleName();

    ImageView backButton, searchButton;
    TextView tvAddNewFriend, tvSendRequest;
    RecyclerView rvSelectFriend, rvRequestStatus;
    SelectFriendAdapter mAdapter;
    LinearLayoutManager mHorizontalManager, mVerticalManager;
    private ArrayList<FriendData> friendDataArrayList;
    FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    DatabaseReference mDatabaseReference;
    ArrayList<FriendData> clickedFriendDataArrayList = new ArrayList<>();
    ArrayList<FriendData> selectedFriendDataArrayList = new ArrayList<>();
    ArrayList<FriendData> receivedFriendDataArrayList = new ArrayList<>();
    ArrayList<FriendData> allRequestedFriendDataArrayList = new ArrayList<>();
    RelativeLayout rlRequestStatus, rlMapHolder;
    TrackRequestAdapter trackRequestAdapter;
    ImageView ivEmptyFriend;
    TextView tvEmptyFriend;
    ArrayList<String> enable_disable = new ArrayList<>();
    RelativeLayout rlMapFragment;

    public static SelectFriendFragment newInstance(Bundle argument){
        SelectFriendFragment fragment = new SelectFriendFragment();
        if(argument != null){
            fragment.setArguments(argument);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_friend_fragment, container, false);
        init(view);
        customizeToolbar(true);
        fetchFriends();
        showMapFragment();

        return view;
    }

    public void init(View view){
        tvAddNewFriend = (TextView) view.findViewById(R.id.tv_add_friend);
        tvSendRequest = (TextView) view.findViewById(R.id.tv_send_request);
        tvSendRequest.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.off_white_color));
        tvSendRequest.setEnabled(false);
        ivEmptyFriend = (ImageView) view.findViewById(R.id.iv_empty_friend);
        ivEmptyFriend.setVisibility(View.GONE);
        tvEmptyFriend = (TextView) view.findViewById(R.id.tv_empty_friend);
        tvEmptyFriend.setVisibility(View.GONE);
        rlRequestStatus = (RelativeLayout) view.findViewById(R.id.rl_request_status);
        rlMapHolder = (RelativeLayout) view.findViewById(R.id.rl_map_holder);
        rvRequestStatus = (RecyclerView) view.findViewById(R.id.rv_request_status);
        rvSelectFriend = (RecyclerView) view.findViewById(R.id.rv_select_friend);
        tvAddNewFriend.setOnClickListener(this);
        tvSendRequest.setOnClickListener(this);
        rlMapFragment = (RelativeLayout) view.findViewById(R.id.rl_map_fragment);
    }

//    public void refreshPage(){
//        firebaseDBHelper.removeValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)));
//        friendDataArrayList.clear();
//        selectedFriendDataArrayList.clear();
//        clickedFriendDataArrayList.clear();
//        allRequestedFriendDataArrayList.clear();
//
//        friendDataArrayList = new ArrayList<>();
//        selectedFriendDataArrayList = new ArrayList<>();
//        clickedFriendDataArrayList = new ArrayList<>();
//        allRequestedFriendDataArrayList.clear();
//
//
//        enable_disable = new ArrayList<>();
//        mAdapter = null;
//        trackRequestAdapter = null;
//
//        fetchFriends();
//    }

    public void fetchFriends(){
        friendDataArrayList = new ArrayList<>();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        firebaseDBHelper.ValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)), AppConstants.TAG_SELECT_FRIEND_FRAGMENT_SELECTED_FRIEND_REFERENCE);

        ValueEventListener valueEventListener = firebaseDBHelper.AddListenerForSingleValueEvent(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_FETCH_FRIEND);
        String loggedInUser = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
        mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(loggedInUser).addListenerForSingleValueEvent(valueEventListener);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_FETCH_FRIEND)){
            boolean flagShowRequestStatusLayout = false;
            DataSnapshot dataSnapshot = ((DataSnapshot) eventBusVO.getData());
            friendDataArrayList.clear();
            for (DataSnapshot ds : dataSnapshot.getChildren()){
                FriendData friendData = ds.getValue(FriendData.class);
                friendDataArrayList.add(friendData);

//                receivedFriendDataArrayList.clear();
                if(friendData.getFriendConnectionStatus() == AppConstants.TRACK_REQUEST_SENT || friendData.getFriendConnectionStatus() == AppConstants.TRACK_ACCEPT_REQUEST || friendData.getFriendConnectionStatus() == AppConstants.TRACK_FRIEND){
//                    receivedFriendDataArrayList.add(friendData);
//
////                    for(FriendData fd : allRequestedFriendDataArrayList){
////                        if(fd.getFriendId().equals(friendData.getFriendId())){
////                            allRequestedFriendDataArrayList.remove(fd);
////                        }
////                    }
////                    allRequestedFriendDataArrayList.add(friendData);
                    flagShowRequestStatusLayout = true;
                }
            }

            if(friendDataArrayList.size() == 0){
                ivEmptyFriend.setVisibility(View.VISIBLE);
                tvEmptyFriend.setVisibility(View.VISIBLE);
            } else {
                ivEmptyFriend.setVisibility(View.GONE);
                tvEmptyFriend.setVisibility(View.GONE);
            }

            setupAdapter();

            if(flagShowRequestStatusLayout){
                setupTrackingAdapter(allRequestedFriendDataArrayList);
            }
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_API_CONNECTION_TRACKING +"Response")){

        }
        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_API_CONNECTION_TRACKING +"Error")){

        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_SELECT_FRIEND_FRAGMENT_SELECTED_FRIEND_REFERENCE)){
            DataSnapshot dataSnapshot = ((DataSnapshot) eventBusVO.getData());
            allRequestedFriendDataArrayList.clear();

            for(DataSnapshot ds : dataSnapshot.getChildren()){
                FriendData friendData = ds.getValue(FriendData.class);
                if(friendData.getFriendConnectionStatus() == AppConstants.TRACK_REQUEST_SENT || friendData.getFriendConnectionStatus() == AppConstants.TRACK_ACCEPT_REQUEST || friendData.getFriendConnectionStatus() == AppConstants.TRACK_FRIEND || friendData.getFriendConnectionStatus() == AppConstants.TRACK_PAUSED){
                    allRequestedFriendDataArrayList.add(friendData);
                }

            }
            setupTrackingAdapter(allRequestedFriendDataArrayList);
            enableDisableFriend();

            //TODO: if any friend unfriend me then update mAdapter also
        }
    }

    public void setupAdapter(){
        if(mAdapter == null){
            if(enable_disable.size() == 0){
                enable_disable.add(0, "enable");
            }

            mAdapter = new SelectFriendAdapter(getActivity(), friendDataArrayList, this, allRequestedFriendDataArrayList, enable_disable);
        } else {
            enable_disable.set(0, "disable");
            mAdapter.notifyDataSetChanged();
        }

        mHorizontalManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvSelectFriend.setLayoutManager(mHorizontalManager);
        rvSelectFriend.setAdapter(mAdapter);

    }

    @Override
    public void onStop() {
        super.onStop();
        for(FriendData friendData: clickedFriendDataArrayList){
            friendData.setFriendConnectionStatus(AppConstants.TRACK_SEND_REQUEST);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        firebaseDBHelper.removeValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_FRIENDS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)));
        selectedFriendDataArrayList.clear();
        customizeToolbar(false);
    }

    public void customizeToolbar(boolean flag){
        if(flag){
            ((BaseActivity) getActivity()).ivBack.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setText("SELECT FRIEND");
            ((BaseActivity) getActivity()).ivSearch.setVisibility(View.VISIBLE);

            backButton = ((BaseActivity) getActivity()).getBackButton();
            searchButton = ((BaseActivity) getActivity()).getSearchButton();
            backButton.setOnClickListener(this);
            searchButton.setOnClickListener(this);
        } else {
            ((BaseActivity) getActivity()).ivBack.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).ivSearch.setVisibility(View.GONE);

        }
    }

    @Override
    public void friendSelected(FriendData friendData, int position, RoundedImageView rivBg) {
        int initialConnectionStatus = friendData.getFriendConnectionStatus();

        if(clickedFriendDataArrayList.contains(friendData)){
            friendData.setFriendConnectionStatus(initialConnectionStatus);
            clickedFriendDataArrayList.remove(clickedFriendDataArrayList.indexOf(friendData));
            rivBg.setVisibility(View.GONE);
        } else {
            friendData.setFriendConnectionStatus(AppConstants.TRACK_REQUEST_SENT);
            clickedFriendDataArrayList.add(friendData);
            rivBg.setVisibility(View.VISIBLE);
        }

        if(clickedFriendDataArrayList.size() > 0){
            tvSendRequest.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.google_red));
            tvSendRequest.setEnabled(true);
        } else {
            tvSendRequest.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.off_white_color));
            tvSendRequest.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back_button:
                ((BaseActivity) getActivity()).onBackPressed();
                break;

            case R.id.iv_search_button:

                break;

            case R.id.tv_add_friend:
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left).replace(R.id.rl_container, AllUsersListFragment.newInstance(null)).addToBackStack(SelectFriendFragment.TAG).commit();
                break;

            case R.id.tv_send_request:
                selectedFriendDataArrayList = clickedFriendDataArrayList;
                for(FriendData friendData : selectedFriendDataArrayList){
                    if(allRequestedFriendDataArrayList.contains(friendData)){
                        allRequestedFriendDataArrayList.remove(friendData);
                    }
                    allRequestedFriendDataArrayList.add(friendData);
                }

                sendTrackingRequest(selectedFriendDataArrayList);

                break;
        }
    }

    public void setupTrackingAdapter(ArrayList<FriendData> allRequestedFriendDataArrayList){
        if(allRequestedFriendDataArrayList.size() > 0){
            rlRequestStatus.setVisibility(View.VISIBLE);
        } else {
            rlRequestStatus.setVisibility(View.GONE);
        }

        if(trackRequestAdapter == null){
            trackRequestAdapter = new TrackRequestAdapter(allRequestedFriendDataArrayList, getActivity(), this);
        } else {
            trackRequestAdapter.notifyDataSetChanged();
        }

        mVerticalManager = new LinearLayoutManager(getActivity());
        rvRequestStatus.setLayoutManager(mVerticalManager);
        rvRequestStatus.setAdapter(trackRequestAdapter);



    }

    public void sendTrackingRequest(ArrayList<FriendData> selectedFriendDataArrayList){

        String name = "";
        String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);

        for (FriendData friendData : selectedFriendDataArrayList){
            String requestRecieverUserToken = friendData.getFriendtoken();
            String requestRecieverUserId = friendData.getFriendId();

            if(AppUtils.isStringValid(name)){
                name = friendData.getFriendName()+", "+name;
            } else {
                name = friendData.getFriendName();
            }

            HashMap <String, String> paramMaps = new HashMap<String, String>();
            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_SEND_TRACKING_REQUEST);

            //Send notification
            CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
            NotificationInput notificationInput = customNotificationBuilder.trackingRequestNotification(requestRecieverUserToken);
            ServiceDelegate.getInstance().getNotificatioResponse(notificationInput, getActivity(), AppConstants.TAG_API_CONNECTION_TRACKING);

        }

        if(AppUtils.isStringValid(name)){
            Toast.makeText(getActivity(), "Tracking request sent to "+name, Toast.LENGTH_LONG).show();
        } else{
            Toast.makeText(getActivity(), "Tracking Request sent", Toast.LENGTH_SHORT).show();
        }

    }

    public void enableDisableFriend(){
        if(enable_disable.size() > 0){
            enable_disable.set(0, "disable");
        } else if(enable_disable.size() == 0){
            enable_disable.add(0, "disable");
        }

        if(mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
    }



    @Override
    public void requestClicked(final FriendData friendData, int position, TextView textView) {
        if(textView.getText().equals(AppConstants.BTN_REQUESTED)){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Remove Connection?");
            builder.setMessage("Do you want to remove tracking connection with " + friendData.friendName +"?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //remove item from track status layout
                            for(FriendData fd : allRequestedFriendDataArrayList){
                                if(fd.getFriendId().equals(friendData.getFriendId())){
                                    allRequestedFriendDataArrayList.remove(fd);
                                    break;
                                }
                            }
                            trackRequestAdapter.notifyDataSetChanged();

                            //enable friend in my connection layout
                            if(enable_disable.size() > 0){
                                enable_disable.set(0, "disable"); //set it disable coz still we want some items to be in disabled mode(which is still in track request layout)
                            } else if(enable_disable.size() == 0){
                                enable_disable.add(0, "disable");
                            }

                            if(mAdapter != null){
                                mAdapter.notifyItemChanged(friendDataArrayList.indexOf(friendData));
                            }


                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST);

                            //TODO: remove connection notification
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(R.drawable.exclamation)
                    .show();
        } else if(textView.getText().equals(AppConstants.BTN_ACCEPT_REJECT)){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Tracking Request");
            builder.setMessage("Allow "+friendData.getFriendName()+" to track your geo-location")
                    .setPositiveButton("ALLOW", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_ACCEPTED_TRACKING_REQUEST);

                            //Send notification
                            CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
                            NotificationInput notificationInput = customNotificationBuilder.acceptTrackingRequestNotification(requestRecieverUserToken);
                            ServiceDelegate.getInstance().getNotificatioResponse(notificationInput, getActivity(), AppConstants.TAG_API_CONNECTION_TRACKING_ACCEPTED);
                        }
                    })
                    .setNegativeButton("REMOVE", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //remove item from track status layout
                            for(FriendData fd : allRequestedFriendDataArrayList){
                                if(fd.getFriendId().equals(friendData.getFriendId())){
                                    allRequestedFriendDataArrayList.remove(fd);
                                    break;
                                }
                            }
                            trackRequestAdapter.notifyDataSetChanged();

                            //enable friend in my connection layout
                            if(enable_disable.size() > 0){
                                enable_disable.set(0, "disable"); //set it disable coz still we want some items to be in disabled mode(which is still in track request layout)
                            } else if(enable_disable.size() == 0){
                                enable_disable.add(0, "disable");
                            }

                            if(mAdapter != null){
                                mAdapter.notifyItemChanged(friendDataArrayList.indexOf(friendData));
                            }


                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST);

                        }
                    })
                    .setIcon(R.drawable.exclamation)
                    .show();
        } else if(textView.getText().equals(AppConstants.BTN_CONNECTED)){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pause or Disconnect");
            builder.setMessage("You can Pause or Disconnect your location tracking by "+friendData.getFriendName())
                    .setPositiveButton("PAUSE", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();
                            String requestRecieverUserName = friendData.getFriendName();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_TRACKING_PAUSED);

                            sendPausedNotification(requestRecieverUserToken, requestRecieverUserName);

                        }
                    })
                    .setNegativeButton("DISCONNECT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //remove item from track status layout
                            for(FriendData fd : allRequestedFriendDataArrayList){
                                if(fd.getFriendId().equals(friendData.getFriendId())){
                                    allRequestedFriendDataArrayList.remove(fd);
                                    break;
                                }
                            }
                            trackRequestAdapter.notifyDataSetChanged();

                            //enable friend in my connection layout
                            if(enable_disable.size() > 0){
                                enable_disable.set(0, "disable"); //set it disable coz still we want some items to be in disabled mode(which is still in track request layout)
                            } else if(enable_disable.size() == 0){
                                enable_disable.add(0, "disable");
                            }

                            if(mAdapter != null){
                                mAdapter.notifyItemChanged(friendDataArrayList.indexOf(friendData));
                            }


                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST);

                        }
                    })
                    .setIcon(R.drawable.exclamation)
                    .show();
        } else if(textView.getText().equals(AppConstants.BTN_PAUSED)){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Resume Connection");
            builder.setMessage("Do you want to resume connection with "+friendData.getFriendName()+". Location tracking will be restarted.")
                    .setPositiveButton("RESUME", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();
                            String requestRecieverUserName = friendData.getFriendName();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_ACCEPTED_TRACKING_REQUEST);

                            sendResumeNotification(requestRecieverUserToken, requestRecieverUserName);

                        }
                    })
                    .setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("DISCONNECT", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //remove item from track status layout
                            for(FriendData fd : allRequestedFriendDataArrayList){
                                if(fd.getFriendId().equals(friendData.getFriendId())){
                                    allRequestedFriendDataArrayList.remove(fd);
                                    break;
                                }
                            }
                            trackRequestAdapter.notifyDataSetChanged();

                            //enable friend in my connection layout
                            if(enable_disable.size() > 0){
                                enable_disable.set(0, "disable"); //set it disable coz still we want some items to be in disabled mode(which is still in track request layout)
                            } else if(enable_disable.size() == 0){
                                enable_disable.add(0, "disable");
                            }

                            if(mAdapter != null){
                                mAdapter.notifyItemChanged(friendDataArrayList.indexOf(friendData));
                            }


                            //update database
                            String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                            String requestRecieverUserToken = friendData.getFriendtoken();
                            String requestRecieverUserId = friendData.getFriendId();

                            HashMap <String, String> paramMaps = new HashMap<String, String>();
                            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST);

                        }
                    })
                    .setIcon(R.drawable.exclamation)
                    .show();
        }


    }

    public void sendPausedNotification(final String requestRecieverUserToken, final String requestReceiverUserName){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Send Notification");
        builder.setMessage("Location tracking is paused. Do you want to send paused-notification to "+requestReceiverUserName +"?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //Send notification
                        CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
                        NotificationInput notificationInput = customNotificationBuilder.trackingPausedNotification(requestRecieverUserToken);
                        ServiceDelegate.getInstance().getNotificatioResponse(notificationInput, getActivity(), AppConstants.TAG_API_CONNECTION_TRACKING_PAUSED);
                        Toast.makeText(getActivity(), "Paused notification sent to "+requestReceiverUserName, Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.exclamation)
                .show();

    }

    public void sendResumeNotification(final String requestRecieverUserToken, final String requestReceiverUserName){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Send Notification");
        builder.setMessage("Location tracking is resumed. Do you want to send resumed tracking notification to "+requestReceiverUserName +"?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //Send notification
                        CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
                        NotificationInput notificationInput = customNotificationBuilder.trackingResumedNotification(requestRecieverUserToken);
                        ServiceDelegate.getInstance().getNotificatioResponse(notificationInput, getActivity(), AppConstants.TAG_API_CONNECTION_TRACKING_RESUMED);
                        Toast.makeText(getActivity(), "Resumed notification sent to "+requestReceiverUserName, Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.exclamation)
                .show();

    }

    public void showMapFragment(){
        getChildFragmentManager().beginTransaction().add(R.id.rl_map_fragment, MapFragment.newInstance(null)).commit();
    }
}
