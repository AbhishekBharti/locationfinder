package com.example.ziffi.locationfinderfirebase.Utils;

/**
 * Created by ziffi on 11/12/16.
 */

public class AppConstants {

    //ConnectionStatus
    public static final int REQUEST_TO_CONNECT = 0;
    public static final int SENT_REQUEST = 1;
    public static final int RECEIVED_REQUEST = 2;
    public static final int FRIEND = 3;

    public static final int TRACK_SEND_REQUEST = 4;
    public static final int TRACK_REQUEST_SENT = 5;
    public static final int TRACK_ACCEPT_REQUEST = 6;
    public static final int TRACK_FRIEND = 7;
    public static final int TRACK_PAUSED = 8;

    public static final String REQUEST_SENDER_ID = "request_sender_id";
    public static final String REQUEST_RECEIVER_ID = "request_receiver_id";

    //button text
    public static final String BTN_CONNECT = "Connect";
    public static final String BTN_PENDING = "Pending";
    public static final String BTN_ACCEPT_REJECT = "Accept/Reject";
    public static final String BTN_FRIEND = "Friend";
    public static final String BTN_REQUESTED = "Requested";
    public static final String BTN_CONNECTED = "Connected";
    public static final String BTN_PAUSED = "Paused";


    //shared pref
    public static final String IS_USER_LOGGEDIN = "is_user_loggedin";
    public static final String LOGGEDIN_USER_ID = "loggedin_user_id";
    public static final String LOGGEDIN_USER_NAME = "loggedin_user_name";
    public static final String LOGGEDIN_USER_EMAIL = "loggedin_user_email";
    public static final String LOGGEDIN_USER_PROFILE_PIC = "loggedin_user_profile_pic";
    public static final String LOGGEDIN_USER_TOKEN = "loggedin_user_token";

    public static final String LOGGEDIN_USER_FRIEND_ID = "loggedin_user_friend_id";
    public static final String LOGGEDIN_USER_FRIEND_NAME = "loggedin_user_friend_name";
    public static final String LOGGEDIN_USER_FRIEND_EMAIL = "loggedin_user_friend_email";
    public static final String LOGGEDIN_USER_FRIEND_PROFILE_PIC = "loggedin_user_friend_profile_pic";
    public static final String LOGGEDIN_USER_FRIEND_TOKEN = "loggedin_user_friend_token";

    //tags
    public static final String TAG_LOGINFRAGMENT_CREATENEWACCOUNT = "LoginFragment_CreateNewAccount";
    public static final String TAG_LOGINFRAGMENT_LOGGEDIN_USER_UPDATE = "LoginFragment_LoggedinUserUpdate";
    public static final String TAG_LOGINFRAGMENT_UPDATE_TOKEN = "LoginFragment_Update_Token";
    public static final String TAG_LOGINFRAGMENT_ADDLISTENER_FORSINGLEVALUEEVENT = "LoginFragment_AddListenerForSingleValueEvent";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_FETCH_ALL_USERS = "AllUsersListFragment_FetchAllUsers";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_FETCH_CONNECTION_STATUS = "AllUsersListFragment_FetchConnectionStatus";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT = "AllUsersListFragment_ConnectFriendRequestSent";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_RECEIVED = "AllUsersListFragment_ConnectFriendRequestReceived";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_ACCEPTED = "AllUsersListFragment_ConnectFriendRequestAccepted";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_CLICKED = "AllUsersListFragment_ConnectFriendClicked";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_CANCEL = "AllUsersListFragment_ConnectFriendRequestCancel";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_USER_CONNECTION_REFERENCE = "AllUserListFragment_UserConnectionReference";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_ADD_FRIEND = "AllUsersListFragment_AddFriend";
    public static final String TAG_ALL_USERS_LIST_FRAGMENT_REMOVE_FRIEND = "AllUsersListFragment_RemoveFriend";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_FETCH_FRIEND = "SelectFriendFragment_FetchFriend";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_SEND_TRACKING_REQUEST = "SelectFriendFragment_SendTrackingRequest";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_REMOVE_TRACKING_REQUEST = "SelectFriendFragment_RemoveTrackingRequest";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_ACCEPTED_TRACKING_REQUEST = "SelectFriendFragment_AcceptedTrackingRequest";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_SELECTED_FRIEND_REFERENCE = "SelectFriendFragment_SelectedFriendReference";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_TRACKING_PAUSED = "SelectFriendFragment_TrackingPaused";
    public static final String TAG_SELECT_FRIEND_FRAGMENT_TRACKING_RESUMED = "SelectFriendFragment_TrackingResumed";



    public static final String TAG_API_CONNECTION_NOTIFICATION = "ApiConnectionNotification";
    public static final String TAG_API_CONNECTION_TRACKING = "ApiConnectionTracking";
    public static final String TAG_API_CONNECTION_TRACKING_ACCEPTED = "ApiConnectionTrackingAccepted";
    public static final String TAG_API_CONNECTION_TRACKING_PAUSED = "ApiConnectionTrackingPaused";
    public static final String TAG_API_CONNECTION_TRACKING_RESUMED = "ApiConnectionTrackingResumed";

}
