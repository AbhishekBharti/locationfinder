package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/7/16.
 */

public class Comment {
    private String userName;
    private String commentText;

    public Comment(){

    }

    public Comment(String userName, String commentText) {
        this.userName = userName;
        this.commentText = commentText;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
