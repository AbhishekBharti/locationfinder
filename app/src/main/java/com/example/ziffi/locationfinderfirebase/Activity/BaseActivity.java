package com.example.ziffi.locationfinderfirebase.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Fragment.AllUsersListFragment;
import com.example.ziffi.locationfinderfirebase.Fragment.BaseFragment;
import com.example.ziffi.locationfinderfirebase.Fragment.SelectFriendFragment;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


/**
 * Created by ziffi on 11/3/16.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    ActionBar actionBar;
    View appBarView;
    public static Toolbar toolbar;
    public static ImageView ivBack, ivSelect, ivSearch;
    public static TextView tvTitle;
    public static RelativeLayout rlLoaderEffect;
    public static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //For app in full screen
        setContentView(R.layout.activity_base); // an example activity_main.xml is provided below
        rlLoaderEffect = (RelativeLayout) findViewById(R.id.rl_loading_effect);
        progressBar = (ProgressBar) findViewById(R.id.pb_loading_indicator);

        setupToolbar();

    }


    public void setupToolbar(){
        // Always cast your custom Toolbar here, and set it as the ActionBar.
        appBarView = (View) findViewById(R.id.include_appbar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivBack = (ImageView) findViewById(R.id.iv_back_button);
        ivBack.setOnClickListener(this);
        ivSelect = (ImageView) findViewById(R.id.iv_select_button);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        ivSearch = (ImageView) findViewById(R.id.iv_search_button);
        ivSearch.setOnClickListener(this);


        setSupportActionBar(toolbar);

        // Get the ActionBar here to configure the way it behaves.
        actionBar = getSupportActionBar();
        //actionBar.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        actionBar.setDisplayShowHomeEnabled(false); // show or hide the default home button
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(false); // enable overriding the default toolbar layout
        actionBar.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
    }

    public void hideActionBar(){
        if(actionBar != null){
            actionBar.hide();
            Log.e("hideActionbar", "called");
        }
    }

    public void showActionBar(){
        if(actionBar != null){
            actionBar.show();
            Log.e("showActionbar", "called");
        }
    }

    public ImageView getBackButton(){
        return ivBack;
    }

    public ImageView getSearchButton(){
        return ivSearch;
    }

    public void showLoaderEffect(){
        if(rlLoaderEffect != null){
            progressBar.setVisibility(View.VISIBLE);
            rlLoaderEffect.setVisibility(View.VISIBLE);
        }
    }

    public void hideLoaderEffect(){
        if(rlLoaderEffect != null){
            progressBar.setVisibility(View.GONE);
            rlLoaderEffect.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Fragment selectFriend = getSupportFragmentManager().findFragmentByTag(SelectFriendFragment.TAG);
        Fragment AllUserListFragment = getSupportFragmentManager().findFragmentByTag(AllUsersListFragment.TAG);

        if(selectFriend != null && selectFriend.isVisible()){
            getSupportFragmentManager().popBackStack();
        } else if(AllUserListFragment != null && AllUserListFragment.isVisible()){
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back_button:
                Toast.makeText(BaseActivity.this, "Back is pressed", Toast.LENGTH_SHORT).show();
                break;

            case R.id.iv_search_button:
                Toast.makeText(BaseActivity.this, "Search is pressed", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
