package com.example.ziffi.locationfinderfirebase.Utils;

import android.app.Application;
import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by ziffi on 11/12/16.
 */

public class LocationFinderApplication extends Application{

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static void setBooleanSharedPreference(String key, boolean value){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putBoolean(key, value).commit();
    }

    public static boolean getBooleanSharedPreference(String key){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(key, false);
    }

    public static void setStringSharedPreference(String key, String value){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).commit();
    }

    public static String getStringSharedPreference(String key){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, null);
    }
}
