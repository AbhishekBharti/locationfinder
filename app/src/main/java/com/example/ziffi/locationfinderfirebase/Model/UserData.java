package com.example.ziffi.locationfinderfirebase.Model;

import android.net.Uri;

/**
 * Created by ziffi on 11/10/16.
 */

public class UserData {
    public String userId;
    public String userName;
    public String userEmail;
    public String profilePic;
    public String userToken;

    public UserData(){}

    public UserData(String userId, String userName, String userEmail, String profilePic, String userToken) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.profilePic = profilePic;
        this.userToken = userToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
