package com.example.ziffi.locationfinderfirebase.Fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.R;
import com.google.firebase.auth.FirebaseAuth;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ziffi on 11/3/16.
 */

public class BaseFragment extends Fragment {
    public final String TAG = BaseFragment.class.getSimpleName();


    public String getLoggedInUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
    public String getLoggedInUserName(){
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    }
    public String getLoggedInUserEmail(){
        return FirebaseAuth.getInstance().getCurrentUser().getEmail();
    }

    public void fragTansaction(Fragment initFragment, Fragment finalFragment, String tag){
        getFragmentManager().beginTransaction().replace(R.id.rl_container, finalFragment, tag).addToBackStack(null).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){

    }



}
