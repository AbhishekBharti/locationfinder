package com.example.ziffi.locationfinderfirebase.Model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ziffi on 11/4/16.
 */
@IgnoreExtraProperties
public class User {
    public String username;
    public String email;

    public User(){

    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }
}
