package com.example.ziffi.locationfinderfirebase.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.FriendData;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Widget.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ziffi on 11/15/16.
 */

public class SelectFriendAdapter extends RecyclerView.Adapter<SelectFriendAdapter.ViewHolder> {

    public interface SelectFriend {
        void friendSelected(FriendData friendData, int position, RoundedImageView rivBg);
    }

    private Context mContext;
    private ArrayList<FriendData> friendDataArrayList = new ArrayList<>();
    private SelectFriend selectFriend;
    private ArrayList<FriendData> allRequestedFriendDataArrayList = new ArrayList<>();
    private ArrayList<String> enable_disable = new ArrayList<>();

    public SelectFriendAdapter(Context mContext, ArrayList<FriendData> friendDataArrayList, SelectFriend selectFriend, ArrayList<FriendData> allRequestedFriendDataArrayList, ArrayList<String> enable_disable) {
        this.mContext = mContext;
        this.friendDataArrayList = friendDataArrayList;
        this.selectFriend = selectFriend;
        this.allRequestedFriendDataArrayList = allRequestedFriendDataArrayList;
        this.enable_disable = enable_disable;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.select_friend_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(mContext).load(friendDataArrayList.get(position).getFriendProfilePic()).into(holder.rivFriendPic);
        holder.tvFriendName.setText(friendDataArrayList.get(position).getFriendName());
        if(position == friendDataArrayList.size()-1){
            holder.vDivider.setVisibility(View.GONE);
        } else {
            holder.vDivider.setVisibility(View.VISIBLE);
        }

        if (friendDataArrayList.get(position).getFriendConnectionStatus() == AppConstants.TRACK_REQUEST_SENT  || friendDataArrayList.get(position).getFriendConnectionStatus() == AppConstants.TRACK_ACCEPT_REQUEST || friendDataArrayList.get(position).getFriendConnectionStatus() == AppConstants.TRACK_FRIEND){
            holder.rivFriendSelectedBg.setVisibility(View.VISIBLE);
            holder.rivFriendSelectedBg.setAlpha(0.2f);
            holder.rivFriendPic.setAlpha(0.5f);
            holder.tvFriendName.setAlpha(0.5f);
            holder.itemView.setEnabled(false);

        } else {
            holder.rivFriendSelectedBg.setVisibility(View.GONE);
            holder.rivFriendSelectedBg.setAlpha(1f);
            holder.rivFriendPic.setAlpha(1f);
            holder.tvFriendName.setAlpha(1f);
            holder.itemView.setEnabled(true);
        }

        if(enable_disable.get(0).equals("disable")){

            holder.rivFriendSelectedBg.setVisibility(View.GONE);
            holder.rivFriendSelectedBg.setAlpha(1f);
            holder.rivFriendPic.setAlpha(1f);
            holder.tvFriendName.setAlpha(1f);
            holder.itemView.setEnabled(true);

            for(FriendData friendData : allRequestedFriendDataArrayList){
                if(friendData.getFriendId().equals(friendDataArrayList.get(position).getFriendId())){
                    holder.rivFriendSelectedBg.setVisibility(View.VISIBLE);
                    holder.rivFriendSelectedBg.setAlpha(0.2f);
                    holder.rivFriendPic.setAlpha(0.5f);
                    holder.tvFriendName.setAlpha(0.5f);
                    holder.itemView.setEnabled(false);
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return friendDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RoundedImageView rivFriendPic, rivFriendSelectedBg;
        TextView tvFriendName;
        View vDivider;
        public ViewHolder(View itemView) {
            super(itemView);

            vDivider = (View) itemView.findViewById(R.id.v_divider);
            rivFriendPic = (RoundedImageView) itemView.findViewById(R.id.riv_select_friend_pic);
            tvFriendName = (TextView) itemView.findViewById(R.id.tv_select_friend_name);
            rivFriendSelectedBg = (RoundedImageView) itemView.findViewById(R.id.riv_selected_friend_bg);
            rivFriendSelectedBg.setVisibility(View.GONE);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            selectFriend.friendSelected(friendDataArrayList.get(getAdapterPosition()), getAdapterPosition(), rivFriendSelectedBg);
        }
    }
}
