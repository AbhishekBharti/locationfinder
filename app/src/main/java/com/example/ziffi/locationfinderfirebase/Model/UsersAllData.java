package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/17/16.
 */

public class UsersAllData {
    public String userId;
    public String userName;
    public String userEmail;
    public String profilePic;
    public String userToken;
    public String friendId;
    public int connectionStatus;

    public UsersAllData(){}

    public UsersAllData(String userId, String userName, String userEmail, String profilePic, String userToken, String friendId, int connectionStatus) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.profilePic = profilePic;
        this.userToken = userToken;
        this.friendId = friendId;
        this.connectionStatus = connectionStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public int getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(int connectionStatus) {
        this.connectionStatus = connectionStatus;
    }
}
