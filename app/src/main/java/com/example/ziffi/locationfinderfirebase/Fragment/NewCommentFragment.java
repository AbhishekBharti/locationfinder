package com.example.ziffi.locationfinderfirebase.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Adapter.NewCommentAdapter;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.NewComment;
import com.example.ziffi.locationfinderfirebase.Model.Post;
import com.example.ziffi.locationfinderfirebase.Model.User;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by ziffi on 11/7/16.
 */

public class NewCommentFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = NewCommentFragment.class.getSimpleName();

    FirebaseDatabase mDatabase;
    DatabaseReference mPostReference, mCommentReference;
    ValueEventListener mPostListener;

    TextView tvPostUsername, tvPostTitle, tvPostBody;
    RecyclerView rvComments;
    EditText etComment;
    Button btnPost;
    FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    NewCommentAdapter newCommentAdapter;

    public static NewCommentFragment newInstance(Bundle argument){
        NewCommentFragment fragment = new NewCommentFragment();
        if(argument != null){
            fragment.setArguments(argument);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_comment_fragment, container, false);
        init(view);
        return view;
    }

    public void init(View view){
        tvPostUsername = (TextView) view.findViewById(R.id.tv_post_username);
        tvPostTitle = (TextView) view.findViewById(R.id.tv_post_title);
        tvPostBody = (TextView) view.findViewById(R.id.tv_post_body);
        rvComments = (RecyclerView) view.findViewById(R.id.rv_comments);
        etComment = (EditText) view.findViewById(R.id.et_comment);
        btnPost = (Button) view.findViewById(R.id.btn_submit);
        btnPost.setOnClickListener(this);
        rvComments.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();
        String mPostKey = getArguments().getString("post_key");
        if(mPostKey == null){
            throw new IllegalArgumentException("Must pass post key");
        }

        mPostReference = FirebaseDatabase.getInstance().getReference().child(FirebaseDBAttributes.TABLE_POSTS).child(mPostKey);
        mCommentReference = FirebaseDatabase.getInstance().getReference().child(FirebaseDBAttributes.TABLE_POSTS_COMMENTS).child(mPostKey);


        firebaseDBHelper.ValueEventListener(mPostReference, "newCommentFragment");

        newCommentAdapter = new NewCommentAdapter(getContext(), mCommentReference);
        rvComments.setAdapter(newCommentAdapter);


    }

    @Override
    public void onStop() {
        super.onStop();
        newCommentAdapter.cleanupListener();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if(i == R.id.btn_submit){
            postComment();
        }
    }

    private void postComment(){
        final String uid = "1";
        ValueEventListener valueEventListener = firebaseDBHelper.AddListenerForSingleValueEvent("newCommentFragment_addListenerForSingleValueEvent");
        FirebaseDatabase.getInstance().getReference().child("USERS").child("1").addListenerForSingleValueEvent(valueEventListener);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){
        if(eventBusVO != null && eventBusVO.getTag().equals("newCommentFragment")){
            Post post = ((DataSnapshot) eventBusVO.getData()).getValue(Post.class);
            tvPostUsername.setText(post.author.toString());
            tvPostTitle.setText(post.title.toString());
            tvPostBody.setText(post.body.toString());

        }

        if(eventBusVO != null && eventBusVO.getTag().equals("newCommentFragment_addListenerForSingleValueEvent")){
            User user = ((DataSnapshot) eventBusVO.getData()).getValue(User.class);
            String authorName = user.username;

            //Create New Comment Object
            String commentText = etComment.getText().toString();
            NewComment newComment = new NewComment("1", authorName, commentText);
            mCommentReference.push().setValue(newComment);
            etComment.setText(null);

        }
    }
}
