package com.example.ziffi.locationfinderfirebase.Network;

import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Model.NotificationResponse;

import retrofit2.Call;

/**
 * Created by ziffi on 11/19/16.
 */

public class NetworkDAO {

    private static NetworkDAO instance;
    private final ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

    public static NetworkDAO getInstance(){
        if (instance == null){
            instance = new NetworkDAO();
        }
        return instance;
    }

    public Call<NotificationResponse> getNotificatioResponse(NotificationInput notificationInput){
        Call<NotificationResponse> call = apiService.getNotificationResponse(notificationInput);
        return call;

    }
}
