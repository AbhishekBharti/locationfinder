package com.example.ziffi.locationfinderfirebase.Utils;

/**
 * Created by ziffi on 11/5/16.
 */

public class FirebaseDBAttributes {
    //TABLES
//    public static final String TABLE_USERS = "USERS";
    public static final String TABLE_POSTS = "POSTS";
    public static final String TABLE_USERS_POSTS = "USERS_POSTS";
    public static final String TABLE_COMMENTS = "COMMENTS";
    public static final String TABLE_POSTS_COMMENTS = "POSTS_COMMENTS";

    public static final String TABLE_USERS = "USERS";
    public static final String TABLE_USERS_CONNECTIONS = "USERS_CONNECTIONS";
    public static final String TABLE_USERS_FRIENDS = "USERS_FRIENDS";
    //COLUMNS

}
