package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/17/16.
 */

public class NotificationParams {
    final String body;
    final String title;

    public NotificationParams(String body, String title) {
        this.body = body;
        this.title = title;
    }
}
