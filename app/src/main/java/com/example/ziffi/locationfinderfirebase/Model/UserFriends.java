package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/13/16.
 */

public class UserFriends {
    private String friendId;
    private String friendName;
    private String friendPic;

    public UserFriends(){}

    public UserFriends(String friendId, String friendName, String friendPic) {
        this.friendId = friendId;
        this.friendName = friendName;
        this.friendPic = friendPic;
    }
}
