package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/7/16.
 */

public class NewComment {
    public String uid;
    public String authorName;
    public String commentText;

    public NewComment(){}

    public NewComment(String uid, String authorName, String commentText) {
        this.uid = uid;
        this.authorName = authorName;
        this.commentText = commentText;
    }


}
