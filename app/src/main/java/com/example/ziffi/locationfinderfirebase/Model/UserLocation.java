package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/8/16.
 */

public class UserLocation {
    String user;
    double lat;
    double lng;

    public UserLocation(){}

    public UserLocation(String user, double lat, double lng) {
        this.user = user;
        this.lat = lat;
        this.lng = lng;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
