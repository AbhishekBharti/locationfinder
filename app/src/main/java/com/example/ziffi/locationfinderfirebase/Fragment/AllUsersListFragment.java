package com.example.ziffi.locationfinderfirebase.Fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziffi.locationfinderfirebase.Activity.BaseActivity;
import com.example.ziffi.locationfinderfirebase.Adapter.AllUserSearchAdapter;
import com.example.ziffi.locationfinderfirebase.Model.ConnectionStatus;
import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.Model.NotificationData;
import com.example.ziffi.locationfinderfirebase.Model.NotificationInput;
import com.example.ziffi.locationfinderfirebase.Model.NotificationParams;
import com.example.ziffi.locationfinderfirebase.Model.NotificationResponse;
import com.example.ziffi.locationfinderfirebase.Model.UserData;
import com.example.ziffi.locationfinderfirebase.Model.UsersAllData;
import com.example.ziffi.locationfinderfirebase.Network.ApiClient;
import com.example.ziffi.locationfinderfirebase.Network.ApiInterface;
import com.example.ziffi.locationfinderfirebase.Network.NetworkDAO;
import com.example.ziffi.locationfinderfirebase.Network.ServiceDelegate;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.AppConstants;
import com.example.ziffi.locationfinderfirebase.Utils.AppUtils;
import com.example.ziffi.locationfinderfirebase.Utils.CustomNotificationBuilder;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBAttributes;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.example.ziffi.locationfinderfirebase.Utils.LocationFinderApplication;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.x;

/**
 * Created by ziffi on 11/14/16.
 */

public class AllUsersListFragment extends BaseFragment implements AllUserSearchAdapter.ConnectionRequest, View.OnClickListener {
    public static final String TAG = AllUsersListFragment.class.getSimpleName();

    FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    DatabaseReference mDatabaseReference;

    private ArrayList<UserData> userDataArrayList = new ArrayList<>();
    private ArrayList<ConnectionStatus> userConnectionStatusArrayList = new ArrayList<>();
    private ArrayList<UsersAllData> usersAllDataArrayList = new ArrayList<>();
    private EditText etSearchFriendText;
    private RecyclerView rvSearchFriendList;
    private AllUserSearchAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ImageView backButton;
    int entryForSetupAdapter = 0, positionClicked = -1;
    Map<String, ConnectionStatus> dbAllConnectedUsers;
    String connectionRequestRecieverUserId;
    private SwipeRefreshLayout swipeContainer;


    public static AllUsersListFragment newInstance(Bundle argument){
        AllUsersListFragment fragment = new AllUsersListFragment();
        if(argument != null){
            fragment.setArguments(argument);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_users_list_fragment, container, false);
        etSearchFriendText = (EditText) view.findViewById(R.id.et_search_friend_text);
        rvSearchFriendList = (RecyclerView) view.findViewById(R.id.rv_search_friend_list);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPage();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);



        fetchAllUsers();

        return view;
    }

    public void refreshPage(){
        firebaseDBHelper.removeValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)));
        entryForSetupAdapter = 0;
        positionClicked = -1;
        userDataArrayList = new ArrayList<>();
        userConnectionStatusArrayList = new ArrayList<>();
        usersAllDataArrayList = new ArrayList<>();
        mAdapter = null;
        dbAllConnectedUsers = null;
        connectionRequestRecieverUserId = null;
        fetchAllUsers();
    }

    public void fetchAllUsers(){
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS).addListenerForSingleValueEvent(firebaseDBHelper.AddListenerForSingleValueEvent(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_ALL_USERS));
        firebaseDBHelper.ValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)), AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_USER_CONNECTION_REFERENCE);
        mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)).addListenerForSingleValueEvent(firebaseDBHelper.AddListenerForSingleValueEvent(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_CONNECTION_STATUS));

        swipeContainer.setRefreshing(false);

    }

    @Override
    public void onStart() {
        super.onStart();
        customizeToolbar(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebaseDBHelper.removeValueEventListener(mDatabaseReference.child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID)));
        customizeToolbar(false);
    }

    public void customizeToolbar(boolean flag){

        if(flag){
            ((BaseActivity) getActivity()).ivBack.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).tvTitle.setText("ADD NEW CONNECTION");

            backButton = ((BaseActivity) getActivity()).getBackButton();

            backButton.setOnClickListener(this);
        } else {
            ((BaseActivity) getActivity()).ivBack.setVisibility(View.GONE);
            ((BaseActivity) getActivity()).tvTitle.setVisibility(View.GONE);

        }

    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){
        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_ALL_USERS)){
            DataSnapshot dataSnapshot = ((DataSnapshot) eventBusVO.getData());
            for(DataSnapshot UserDatashot: dataSnapshot.getChildren()){
                UserData userData = UserDatashot.getValue(UserData.class);
                userDataArrayList.add(userData);
            }
            entryForSetupAdapter++;
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_CONNECTION_STATUS)){
            DataSnapshot dataSnapshot = ((DataSnapshot) eventBusVO.getData());
            for(DataSnapshot connectionStatus: dataSnapshot.getChildren()){
                ConnectionStatus status = connectionStatus.getValue(ConnectionStatus.class);
                userConnectionStatusArrayList.add(status);
            }
            dbAllConnectedUsers = (Map<String, ConnectionStatus>)((DataSnapshot) eventBusVO.getData()).getValue();
            entryForSetupAdapter++;
        }

        if(entryForSetupAdapter == 2 && (eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_ALL_USERS) || eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_FETCH_CONNECTION_STATUS))){
            for(int i = 0; i< userDataArrayList.size(); i++){
                String userId = userDataArrayList.get(i).getUserId();
                String userName = userDataArrayList.get(i).getUserName();
                String userEmail = userDataArrayList.get(i).getUserEmail();
                String profilePic = userDataArrayList.get(i).getProfilePic();
                String userToken = userDataArrayList.get(i).getUserToken();

                UsersAllData usersAllData;
                if(dbAllConnectedUsers != null && dbAllConnectedUsers.size() > 0 && dbAllConnectedUsers.containsKey(userId)){
                    int connectionStatus = -1;
                    for (ConnectionStatus css : userConnectionStatusArrayList){
                        int c = css.getConnectionStatus();
                        String s = css.getFriendId();

                        if(s!= null && s.equals(userId)){
                            connectionStatus = c;
                            break;
                        }
                    }

                    usersAllData = new UsersAllData(userId, userName, userEmail, profilePic, userToken, userId, connectionStatus);
                    usersAllDataArrayList.add(usersAllData);
                } else {
                    usersAllData = new UsersAllData(userId, userName, userEmail, profilePic, userToken, "", AppConstants.REQUEST_TO_CONNECT);
                    usersAllDataArrayList.add(usersAllData);
                }
            }

            setupAdapter();
//            checkChildChange();
        }

//        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT)){
//            if (positionClicked > -1){
//                usersAllDataArrayList.get(positionClicked).setConnectionStatus(AppConstants.SENT_REQUEST);
//                mAdapter.notifyItemChanged(positionClicked, usersAllDataArrayList);
//            }
//        }
//
//        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_CANCEL)){
//            if (positionClicked > -1){
//                usersAllDataArrayList.get(positionClicked).setConnectionStatus(AppConstants.REQUEST_TO_CONNECT);
//                mAdapter.notifyItemChanged(positionClicked, usersAllDataArrayList);
//            }
//        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_USER_CONNECTION_REFERENCE)){
            if (positionClicked > -1){ // code will execute in sender's phone
                DataSnapshot ds1 = (DataSnapshot) eventBusVO.getData();
                int status = 0;
                for (DataSnapshot ds2 : ds1.getChildren()){
                    ConnectionStatus connectionStatus = ds2.getValue(ConnectionStatus.class);
                    if(connectionStatus.getFriendId().equals(usersAllDataArrayList.get(positionClicked).getUserId())){
                        status = connectionStatus.getConnectionStatus();
                        break;
                    }
                }

                usersAllDataArrayList.get(positionClicked).setConnectionStatus(status);
                mAdapter.notifyItemChanged(positionClicked, usersAllDataArrayList);

                if(status == AppConstants.FRIEND){
                    //Add friend info in db

                    addFriendInDB(positionClicked);
//                    HashMap<String, String> paramsMap = new HashMap<>();
//
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_NAME, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME));
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_EMAIL, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_EMAIL));
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_PROFILE_PIC, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_PROFILE_PIC));
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_TOKEN, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_TOKEN));
//
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionClicked).getUserId());
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_NAME, usersAllDataArrayList.get(positionClicked).getUserName());
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_EMAIL, usersAllDataArrayList.get(positionClicked).getUserEmail());
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_PROFILE_PIC, usersAllDataArrayList.get(positionClicked).getProfilePic());
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_TOKEN, usersAllDataArrayList.get(positionClicked).getUserToken());
//
//                    firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_ADD_FRIEND);

                } else if(status == AppConstants.REQUEST_TO_CONNECT) {
                    //Remove friend if exist
                    removeFriendFromDB(positionClicked);
//                    HashMap<String, String> paramsMap = new HashMap<>();
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
//                    paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionClicked).getUserId());
//                    firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_REMOVE_FRIEND);
                }



//                HashMap<String, String> paramMaps = new HashMap<String, String>();
//                final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
//                paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
//                paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, connectionRequestRecieverUserId);
//                DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
//
//                firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_RECEIVED);
            }

            if(positionClicked < 0){ // code will execute in receiver's phone
                DataSnapshot ds3 = (DataSnapshot) eventBusVO.getData();
                String receiverUserId = ds3.getKey();
                String senderUserId = "";
                int newStatus = -1;
                ConnectionStatus cStatus = null;
                for(DataSnapshot ds4 : ds3.getChildren()){
                    cStatus = ds4.getValue(ConnectionStatus.class);

                    if(userConnectionStatusArrayList != null && userConnectionStatusArrayList.size() > 0){
                        for(ConnectionStatus oldConnectionStatus: userConnectionStatusArrayList){
                            if(oldConnectionStatus.getFriendId().equals(cStatus.getFriendId()) && oldConnectionStatus.getConnectionStatus() != cStatus.getConnectionStatus()){
                                senderUserId = cStatus.getFriendId();
                                newStatus = cStatus.getConnectionStatus();
                                break;
                            }

//                            if(!userConnectionStatusArrayList.contains(cStatus)){
//                                senderUserId = cStatus.getFriendId();
//                                newStatus = cStatus.getConnectionStatus();
//                            }
                        }
                    } else {
                        senderUserId = cStatus.getFriendId();
                        newStatus = cStatus.getConnectionStatus();
                    }


                }

                if(cStatus != null && AppUtils.isStringValid(senderUserId)){
                    int positionChanged = -1;
                    for(UsersAllData usersAllData : usersAllDataArrayList){
                        if(usersAllData.getUserId().equals(senderUserId)){
                            positionChanged = usersAllDataArrayList.indexOf(usersAllData);
                            break;
                        }

                    }

                    if(positionChanged > -1){
                        usersAllDataArrayList.get(positionChanged).setConnectionStatus(newStatus);
                        mAdapter.notifyItemChanged(positionChanged, usersAllDataArrayList);

//                        if(newStatus == AppConstants.FRIEND){
//                            //Add friend info in db
//                            HashMap<String, String> paramsMap = new HashMap<>();
//
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_NAME, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME));
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_EMAIL, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_EMAIL));
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_PROFILE_PIC, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_PROFILE_PIC));
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_TOKEN, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_TOKEN));
//
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionChanged).getUserId());
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_NAME, usersAllDataArrayList.get(positionChanged).getUserName());
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_EMAIL, usersAllDataArrayList.get(positionChanged).getUserEmail());
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_PROFILE_PIC, usersAllDataArrayList.get(positionChanged).getProfilePic());
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_TOKEN, usersAllDataArrayList.get(positionChanged).getUserToken());
//
//                            firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_ADD_FRIEND);
//
//                        } else if(newStatus == AppConstants.REQUEST_TO_CONNECT) {
//                            //Remove friend if exist
//                            HashMap<String, String> paramsMap = new HashMap<>();
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
//                            paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionChanged).getUserId());
//                            firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_REMOVE_FRIEND);
//                        }

                    }
                }
            }
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_API_CONNECTION_NOTIFICATION +"Response")){
            HashMap<String, String> paramMaps = new HashMap<String, String>();
            final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
            paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
            paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, connectionRequestRecieverUserId);
            DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

            firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT);
        }

        if(eventBusVO != null && eventBusVO.getTag().equals(AppConstants.TAG_API_CONNECTION_NOTIFICATION +"Error")){
            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
        }
    }

    public void setupAdapter(){
        if(mAdapter == null){
            mAdapter = new AllUserSearchAdapter(usersAllDataArrayList, getActivity(), this);
        } else {
            mAdapter.notifyDataSetChanged();
        }

        mLayoutManager = new LinearLayoutManager(getContext());
        rvSearchFriendList.setLayoutManager(mLayoutManager);
        rvSearchFriendList.setAdapter(mAdapter);
    }

    public void addFriendInDB(int positionClicked){
        HashMap<String, String> paramsMap = new HashMap<>();

        paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
        paramsMap.put(AppConstants.LOGGEDIN_USER_NAME, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME));
        paramsMap.put(AppConstants.LOGGEDIN_USER_EMAIL, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_EMAIL));
        paramsMap.put(AppConstants.LOGGEDIN_USER_PROFILE_PIC, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_PROFILE_PIC));
        paramsMap.put(AppConstants.LOGGEDIN_USER_TOKEN, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_TOKEN));

        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionClicked).getUserId());
        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_NAME, usersAllDataArrayList.get(positionClicked).getUserName());
        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_EMAIL, usersAllDataArrayList.get(positionClicked).getUserEmail());
        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_PROFILE_PIC, usersAllDataArrayList.get(positionClicked).getProfilePic());
        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_TOKEN, usersAllDataArrayList.get(positionClicked).getUserToken());

        firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_ADD_FRIEND);
    }

    public void removeFriendFromDB(int positionClicked){
        HashMap<String, String> paramsMap = new HashMap<>();
        paramsMap.put(AppConstants.LOGGEDIN_USER_ID, LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID));
        paramsMap.put(AppConstants.LOGGEDIN_USER_FRIEND_ID, usersAllDataArrayList.get(positionClicked).getUserId());
        firebaseDBHelper.WriteInDB(paramsMap, mDatabaseReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_REMOVE_FRIEND);
    }

//    public void checkChildChange(){
//
//        final String loggedInUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
//        DatabaseReference userConnectionReference = FirebaseDatabase.getInstance().getReference().child(FirebaseDBAttributes.TABLE_USERS_CONNECTIONS).child(loggedInUserId);
//        firebaseDBHelper.ChildEventListener(userConnectionReference, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_USER_CONNECTION_REFERENCE);
//    }

    @Override
    public void connectUser(String requestRecieverUserToken, final String requestRecieverUserId, int position) {

        positionClicked = position;
        connectionRequestRecieverUserId = requestRecieverUserId;
        final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);

        CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
        NotificationInput  notificationInput = customNotificationBuilder.connectionRequestNotification(requestRecieverUserToken);
        ServiceDelegate.getInstance().getNotificatioResponse(notificationInput, getActivity(), AppConstants.TAG_API_CONNECTION_NOTIFICATION);

        Toast.makeText(getActivity(), "Request sent", Toast.LENGTH_SHORT).show();

//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

//        positionClicked = position;
//        String requestSenderUserName = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_NAME);
//        final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);

//        String notificationBody = "";
//        if(AppUtils.isStringValid(requestSenderUserName)){
//            notificationBody = requestSenderUserName +" wants to connect with you";
//        } else {
//            notificationBody = "You got a new Connection request";
//        }
//
//        NotificationData nData = new NotificationData("5x1", "15:10");
//        String to = requestRecieverUserToken;
//        NotificationParams nParams = new NotificationParams(notificationBody, "Connection Request");
//
//        NotificationInput  notificationInput = new NotificationInput(nData, to, nParams);

//        CustomNotificationBuilder customNotificationBuilder = new CustomNotificationBuilder();
//        NotificationInput  notificationInput = customNotificationBuilder.connectionRequestNotification(requestRecieverUserToken);

//        Call<NotificationResponse> call = apiService.getNotificationResponse(notificationInput);

//        Call<NotificationResponse> call = NetworkDAO.getInstance().getNotificatioResponse(notificationInput);

//        ServiceDelegate.getInstance().getNotificatioResponse(notificationInput);
//        call.enqueue(new Callback<NotificationResponse>() {
//            @Override
//            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
//
//                if (response.code() == 400 ) {
//                    Log.d(TAG, "onResponse - Status : " + response.code());
//                    try {
//                        if (response.errorBody() != null){
//                            Log.e("error 400 :",response.errorBody().string());
//                            Toast.makeText(getActivity(), "Something went wrong!", Toast.LENGTH_SHORT).show();
//                        }
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                } else if(response.body() != null){
//                    response.body().toString();
//
//                    HashMap <String, String> paramMaps = new HashMap<String, String>();
//                    paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
//                    paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
//                    DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
//
//                    firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_SENT);
//
//                } else {
//                    Log.e("Error: ", "Something went wrong!!!");
//                }
//
//
//                Log.e("Call ", call.toString());
//                Log.e("Response ", response.toString());
//            }
//
//            @Override
//            public void onFailure(Call<NotificationResponse> call, Throwable t) {
//                // Log error here since request failed
//                Log.e(TAG, t.toString());
//                Log.e("Response ", "Failed");
//            }
//        });

    }

    @Override
    public void requestedClicked(String requestRecieverUserToken, final String requestRecieverUserId, int position, final TextView tvUserConnectButton) {

        final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
        positionClicked = position;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Cancel Request?");
        builder.setMessage("Do you want to cancel connection request?")
                .setPositiveButton("CANCEL REQUEST", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        HashMap <String, String> paramMaps = new HashMap<String, String>();
                        paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                        paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                        DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                        firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_CANCEL);

                        tvUserConnectButton.setText("Connect");
                        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_connect_bg);
                        tvUserConnectButton.setPadding(40, 15, 40, 15);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tvUserConnectButton.setElevation(5);
                        }

                        Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.exclamation)
                .show();
    }

    @Override
    public void acceptClicked(String requestSenderUserToken, final String requestSenderUserId, final int position, final TextView tvUserConnectButton) {

        final String requestRecieverUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
        positionClicked = position;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Accept or Reject?");
        builder.setMessage("How would you respond to connection request?")
                .setPositiveButton("ACCEPT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        HashMap <String, String> paramMaps = new HashMap<String, String>();
                        paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                        paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                        DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                        firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_ACCEPTED);

                        tvUserConnectButton.setText("Friend");
                        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_friend_bg);
                        tvUserConnectButton.setPadding(40, 15, 40, 15);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tvUserConnectButton.setElevation(0);
                        }

                        addFriendInDB(position);
                        Toast.makeText(getActivity(), "Connection accepted", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("REJECT", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        HashMap <String, String> paramMaps = new HashMap<String, String>();
                        paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                        paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                        DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                        firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_CANCEL);

                        tvUserConnectButton.setText("Connect");
                        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_connect_bg);
                        tvUserConnectButton.setPadding(40, 15, 40, 15);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tvUserConnectButton.setElevation(5);
                        }

                        Toast.makeText(getActivity(), "Rejected", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.exclamation)
                .show();


//        final String requestRecieverUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
//        HashMap <String, String> paramMaps = new HashMap<String, String>();
//        paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
//        paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
//        DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();
//
//        firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_REQUEST_ACCEPTED);
//        Toast.makeText(getActivity(), "Connection accepted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void friendClicked(String requestRecieverUserToken, final String requestRecieverUserId, final String requestRecieverUserName, final int position, final TextView tvUserConnectButton) {

        positionClicked = position;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Remove Friend?");
        builder.setMessage("Do you want to remove " + requestRecieverUserName +" from your friends list?")
                .setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        final String requestSenderUserId = LocationFinderApplication.getStringSharedPreference(AppConstants.LOGGEDIN_USER_ID);
                        HashMap <String, String> paramMaps = new HashMap<String, String>();
                        paramMaps.put(AppConstants.REQUEST_SENDER_ID, requestSenderUserId);
                        paramMaps.put(AppConstants.REQUEST_RECEIVER_ID, requestRecieverUserId);
                        DatabaseReference mRef = FirebaseDatabase.getInstance().getReference();

                        firebaseDBHelper.WriteInDB(paramMaps, mRef, AppConstants.TAG_ALL_USERS_LIST_FRAGMENT_CONNECT_FRIEND_CLICKED);

                        tvUserConnectButton.setText("Connect");
                        tvUserConnectButton.setBackgroundResource(R.drawable.round_corner_connect_bg);
                        tvUserConnectButton.setPadding(40, 15, 40, 15);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            tvUserConnectButton.setElevation(5);
                        }

                        removeFriendFromDB(position);
                        Toast.makeText(getActivity(), "Connection removed", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.exclamation)
                .show();

    }

    @Override
    public void onClick(View v) {
        ((BaseActivity) getActivity()).onBackPressed();
    }
}
