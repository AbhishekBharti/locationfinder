package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/4/16.
 */

public class EventBusVO {
    String tag;
    Object data;

    public EventBusVO(String tag, Object data) {
        this.tag = tag;
        this.data = data;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
