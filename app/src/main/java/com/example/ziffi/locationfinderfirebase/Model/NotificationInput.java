package com.example.ziffi.locationfinderfirebase.Model;

/**
 * Created by ziffi on 11/17/16.
 */

public class NotificationInput {
    final NotificationData data;
    final String to;
    final NotificationParams notification;

    public NotificationInput(NotificationData notificationData, String to, NotificationParams notificationParams) {
        this.data = notificationData;
        this.to = to;
        this.notification = notificationParams;
    }
}
