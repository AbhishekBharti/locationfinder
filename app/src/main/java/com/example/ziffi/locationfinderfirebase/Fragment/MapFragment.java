package com.example.ziffi.locationfinderfirebase.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import com.example.ziffi.locationfinderfirebase.Model.UserLocation;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ziffi.locationfinderfirebase.Model.EventBusVO;
import com.example.ziffi.locationfinderfirebase.R;
import com.example.ziffi.locationfinderfirebase.Utils.FirebaseDBHelper;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ziffi on 11/8/16.
 */

public class MapFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    SupportMapFragment mMapFragment;
    LocationManager mLocationManager;
    String locationProvider;
    Location location;
    DatabaseReference mLocationReference;
    FirebaseDBHelper firebaseDBHelper = new FirebaseDBHelper();
    List<String> userLocationId = new ArrayList<>();
    List<UserLocation> userLocations = new ArrayList<>();
    boolean initialMapZooming = true;
    ChildEventListener childEventListener;
    Marker markerObj;
    PolylineOptions polylineOptions = new PolylineOptions();

    public static MapFragment newInstance(Bundle arg){
        MapFragment fragment = new MapFragment();
        if(arg != null){
            fragment.setArguments(arg);
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false);

        polylineOptions.color(Color.parseColor("#F70085ff"));
        polylineOptions.width(15);
        polylineOptions.visible(true);

        mMapFragment = SupportMapFragment.newInstance(options);
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.rl_map_container, mMapFragment).commit();
        mMapFragment.getMapAsync(this);

        addFirebaseListeners();

        return view;
    }

    @Override //called when getMapAsync is called
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if(mMap != null){
                    LatLng currentLocation = new LatLng(location.getLatitude(),location.getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(currentLocation)
                            .title("Marker at your location")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location_marker));

                    if(markerObj != null){
                        markerObj.remove();
                    }
                    markerObj = mMap.addMarker(markerOptions);

                    CameraUpdate center = CameraUpdateFactory.newLatLng(currentLocation);
                    if(center == null){
                        center = CameraUpdateFactory.newLatLng(new LatLng(0,0));
                    }
                    mMap.moveCamera(center);
                    float currentZoom = mMap.getCameraPosition().zoom;
                    if(initialMapZooming){
                        currentZoom = 12;
                        initialMapZooming = false;
                    }

                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(currentZoom);
                    mMap.animateCamera(zoom);
                }


                UserLocation ul = new UserLocation("ABC", location.getLatitude(), location.getLongitude());
                mLocationReference.push().setValue(ul);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        });

    }

    public void addFirebaseListeners(){
        mLocationReference = FirebaseDatabase.getInstance().getReference().child("LOCATION").child("1");
        firebaseDBHelper.ValueEventListener(mLocationReference, "mapfragment_valueeventlistener");

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                UserLocation location = dataSnapshot.getValue(UserLocation.class);
                userLocationId.add(dataSnapshot.getKey());
                userLocations.add(location);
                tracePath(location);
                Log.e("childAdded ", "true");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                UserLocation location = dataSnapshot.getValue(UserLocation.class);
                String key = dataSnapshot.getKey();
                int commentIndex = userLocationId.indexOf(key);
                if(commentIndex > -1){
                    userLocations.set(commentIndex, location);
                    tracePath(location);
                    Log.e("childChanged ", "true");
                } else {
                    Log.w("newCommentAdapter", "onChildChanged:unknown_child:" + key);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                UserLocation location = dataSnapshot.getValue(UserLocation.class);
                String key = dataSnapshot.getKey();
                int commentIndex = userLocationId.indexOf(key);
                if(commentIndex > -1){
                    userLocationId.remove(commentIndex);
                    userLocations.remove(commentIndex);

                }else {
                    Log.w("newCommentAdapter", "onChildRemoved:unknown_child:" + key);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mLocationReference.addChildEventListener(childEventListener);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        firebaseDBHelper.removeValueEventListener(mLocationReference);
        mLocationReference.removeEventListener(childEventListener);
    }

    @Subscribe
    public void onEvent(EventBusVO eventBusVO){
        if(eventBusVO != null && eventBusVO.getTag().equals("mapfragment_valueeventlistener")){
            UserLocation location = ((DataSnapshot) eventBusVO.getData()).getValue(UserLocation.class);

        }
    }

    public void tracePath(UserLocation userLocation){
        if(mMap == null){
            return;
        }
        polylineOptions.add(new LatLng(userLocation.getLat(), userLocation.getLng()));
        mMap.addPolyline(polylineOptions);
    }

}
