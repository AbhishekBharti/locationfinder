package com.example.ziffi.locationfinderfirebase.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ziffi on 11/16/16.
 */

public class NotificationResponse {

    @SerializedName("multicast_id")
    private String multicastId;

    @SerializedName("success")
    private String success;

    @SerializedName("failure")
    private String failure;

    @SerializedName("canonical_ids")
    private String cannonicalId;
//
//    @SerializedName("results")
//    private List<String> results;


    public String getMulticastId() {
        return multicastId;
    }

    public void setMulticastId(String multicastId) {
        this.multicastId = multicastId;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public String getCannonicalId() {
        return cannonicalId;
    }

    public void setCannonicalId(String cannonicalId) {
        this.cannonicalId = cannonicalId;
    }

//    public List<String> getResults() {
//        return results;
//    }
//
//    public void setResults(List<String> results) {
//        this.results = results;
//    }
}
